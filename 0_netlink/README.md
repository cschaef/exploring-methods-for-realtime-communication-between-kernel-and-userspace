# Netlink Communication Interfaces

## Preperations
```
# sudo dnf install libnl3 libnl3-devel libnl3-doc
# sudo apt-get install libnl-3-dev libnl-genl-3-dev
```

## Build
```
# make build 	# build module
# make install 	# insmod module
# make app 	# build test application
# make test	# run test application
```

## References
https://www.infradead.org/~tgr/libnl/
https://wiki.linuxfoundation.org/networking/generic_netlink_howto
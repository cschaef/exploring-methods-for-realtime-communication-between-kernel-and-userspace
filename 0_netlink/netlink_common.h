/**
 *
 * @references:
 * 	https://elixir.bootlin.com/linux/v4.17.19/source/include/uapi/linux/genetlink.h
 */

#ifndef NETLINK_COMMON
#define NETLINK_COMMON

#include <linux/netlink.h>
#include <linux/version.h>

#ifndef __KERNEL__
	#include <netlink/genl/ctrl.h>
	#include <netlink/genl/family.h>
	#include <netlink/genl/genl.h>
#endif

#define GENL_FAMILY_NAME "NETLINK_FAMILY"
#define GENL_MCGRP0_NAME "genl_mcgrp0"
#define GENL_MCGRP1_NAME "genl_mcgrp1"
#define GENL_MCGRP2_NAME "genl_mcgrp2"

#define GENL_ATTR_MAX	   1
#define GENL_ATTR_MSG_LEN  MAX_DATA_OBJECT_LEN
#define MC_MSG_INTERVAL_MS 5000
#define SCHED_PRIORITY	   90

enum GENL_COMMANDS {
	GENL_CMD_UNSPEC, /* Must NOT use element 0 */
	GENL_CMD_MSG,
};

enum GENL_MULTICAST_GROUPS {
	GENL_MULTICAST_GROUP0,
	GENL_MULTICAST_GROUP1,
	GENL_MULTICAST_GROUP2,
};

enum GENL_ATTRS {
	GENL_ATTR_UNSPEC, /* Must NOT use element 0 */
	GENL_ATTR_MSG,
};

static const struct nla_policy genl_policy[GENL_ATTR_MAX + 1]
	= { [GENL_ATTR_UNSPEC] = { .type = NLA_STRING },
	    [GENL_ATTR_MSG]    = { .type = NLA_STRING,
#ifdef __KERNEL__
				.len = GENL_ATTR_MSG_LEN
#else
				.maxlen = GENL_ATTR_MSG_LEN
#endif
	    } };

#endif
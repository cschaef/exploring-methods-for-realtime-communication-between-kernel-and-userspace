/**
 * @file  0_netlink_lkm.c
 * @brief Kernel module to use netlink sockets for communication
 * @references
 * 	https://github.com/a-zaki/genl_ex
 * 	https://elixir.bootlin.com/linux/latest/source/include/net/genetlink.h
 */

#include <linux/export.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/sched.h>
#include <linux/skbuff.h>
#include <linux/timer.h>
#include <linux/version.h>
#include <net/genetlink.h>
#include <uapi/linux/sched/types.h>

#include "../include/sig_common.h"
#include "netlink_common.h"

#define pr_fmt(fmt) "%s:%s: " fmt, KBUILD_MODNAME, __func__

static void multicast_message (unsigned int group, char *data, size_t data_len);
static int recv_message (struct sk_buff *skb, struct genl_info *info);
static void genl_periodic (struct timer_list *unused);
static int __init run (void);
static void __exit cleanup (void);

static struct timer_list timer;
static struct genl_family genl_family;

/* available multicast groups */
static const struct genl_multicast_group genl_multicast_groups[] = {
	[GENL_MULTICAST_GROUP0] = { .name = GENL_MCGRP0_NAME, },
	[GENL_MULTICAST_GROUP1] = { .name = GENL_MCGRP1_NAME, },
	[GENL_MULTICAST_GROUP2] = { .name = GENL_MCGRP2_NAME, },
};

static const struct genl_ops genl_operations[] = {
	{
		.cmd	= GENL_CMD_MSG,
		.policy = genl_policy,
		.doit	= recv_message,
		.dumpit = NULL,
	},
};

static struct genl_family genl_family = {
	.name	  = GENL_FAMILY_NAME,
	.version  = 1,
	.maxattr  = GENL_ATTR_MAX,
	.netnsok  = false,
	.module	  = THIS_MODULE,
	.ops	  = genl_operations,
	.mcgrps	  = genl_multicast_groups,
	.n_ops	  = ARRAY_SIZE (genl_operations),
	.n_mcgrps = ARRAY_SIZE (genl_multicast_groups),
};

/**
 * Sender handler to transmit a multicast message to a group
 * @param group
 * @param data
 * @param data_len
 */
static void
multicast_message (unsigned int group, char *data, size_t data_len)
{
	void *hdr	    = NULL;
	struct sk_buff *skb = NULL;
	int ret		    = 0;

	if (data_len > MAX_DATA_OBJECT_LEN) {
		pr_err ("Data length to long to send (data_len %d\n", data_len);
		return;
	}

	skb = genlmsg_new (data_len, GFP_ATOMIC);
	if (skb == NULL) {
		pr_err ("Error creating new genl message\n");
		return;
	}

	/* create header */
	hdr = genlmsg_put (skb, 0, 0, &genl_family, GFP_ATOMIC, GENL_CMD_MSG);
	if (hdr == NULL) {
		pr_err ("Error adding a genl header to the message\n");
		return;
	}

	if (nla_put_string (skb, GENL_ATTR_MSG, data) != 0) {
		pr_err ("Error adding data to the message");
		return;
	}

	/* finalize message and send multicast */
	genlmsg_end (skb, hdr);
	genlmsg_multicast (&genl_family, skb, 0, group, GFP_ATOMIC);
}

/**
 * Response to incoming requests from userspace
 * @param incoming message to process
 */
static int
message_handler (struct genl_info *info)
{
	char *incoming = (char *)nla_data (info->attrs[GENL_ATTR_MSG]);
	multicast_message (GENL_MULTICAST_GROUP0, incoming, strlen (incoming));
	return 0;
}

/**
 * Receiver handler for incoming messages, prints data to log
 * @param skb
 * @param info
 * @return 0 if successul
 */
static int
recv_message (struct sk_buff *skb, struct genl_info *info)
{
	if (!info->attrs[GENL_ATTR_MSG]) {
		pr_err ("empty message from %d: %p \n", info->snd_portid,
			info->attrs[GENL_ATTR_MSG]);
		return -EINVAL;
	}
	message_handler (info);
	return 0;
}
/**
 * periodic routine to send a multicast message to various groupd
 */
static void
genl_periodic (struct timer_list *unused)
{
	multicast_message (GENL_MULTICAST_GROUP0, "Hello from Kernelspace 0", 23);
	multicast_message (GENL_MULTICAST_GROUP1, "Hello from Kernelspace 1", 23);
	multicast_message (GENL_MULTICAST_GROUP2, "Hello from Kernelspace 2", 23);

	mod_timer (&timer, jiffies + msecs_to_jiffies (MC_MSG_INTERVAL_MS));
}

static int __init
run (void)
{
	pr_info ("Start\n");

	if (set_scheduling_policy () < 0) {
		pr_err ("cannot set scheduling policy\n");
		// return -1;
	}
	// timer_setup (&timer, genl_periodic, 0);
	// mod_timer (&timer, jiffies + msecs_to_jiffies (MC_MSG_INTERVAL_MS));
	genl_register_family (&genl_family);
	return 0;
}

static void __exit
cleanup (void)
{
	pr_info ("Release\n");
	del_timer (&timer);
	genl_unregister_family (&genl_family);
}

module_init (run);
module_exit (cleanup);

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("Christian Schaefer <cschaefer.itsb-b2018@fh-salzburg.ac.at>");
	
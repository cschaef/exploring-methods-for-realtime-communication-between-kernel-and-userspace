/**
 * @references
 * 	https://github.com/phip1611/generic-netlink-user-kernel-rust
 */

#include <ctype.h>
#include <errno.h>
#include <netlink/attr.h>
#include <netlink/msg.h>
#include <netlink/netlink.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#define PROGRAM_NAME "netlink_application"

#include "../include/app_common.h"
#include "../include/sig_common.h"
#include "netlink_common.h"

static unsigned int mcgroups; /* Mask of groups */
struct nl_sock *g_nlsock = NULL;
struct nl_cb *cb	 = NULL;

/* program functions */
static char *genl_mcgrp_names[3] = {
	GENL_MCGRP0_NAME,
	GENL_MCGRP1_NAME,
	GENL_MCGRP2_NAME,
};

static void
add_group (char *group)
{
	unsigned int grp = strtoul (group, NULL, 10);

	if (grp > sizeof (genl_mcgrp_names)) {
		fprintf (stderr, "Invalid group number %u. Values allowed 0:%u\n", grp,
			 (uint)sizeof (genl_mcgrp_names));
		exit (EXIT_FAILURE);
	}

	mcgroups |= 1 << (grp);
}

static int
send_msg_to_kernel (struct nl_sock *sock, const char *message)
{
	// printf ("%s: sending <%s>\n", __func__, message);

	struct nl_msg *msg;
	int family_id, err = 0;

	family_id = genl_ctrl_resolve (sock, GENL_FAMILY_NAME);
	if (family_id < 0) {
		fprintf (stderr, "Unable to resolve family name (%d)!\n", family_id);
		exit (EXIT_FAILURE);
	}

	msg = nlmsg_alloc ();
	if (!msg) {
		fprintf (stderr, "failed to allocate netlink message\n");
		exit (EXIT_FAILURE);
	}

	if (!genlmsg_put (msg, NL_AUTO_PORT, NL_AUTO_SEQ, family_id, 0, NLM_F_REQUEST, GENL_CMD_MSG,
			  0)) {
		fprintf (stderr, "failed to put nl hdr!\n");
		err = -ENOMEM;
		goto out;
	}

	err = nla_put_string (msg, GENL_ATTR_MSG, message);
	if (err) {
		fprintf (stderr, "failed to put nl string!\n");
		goto out;
	}

	err = nl_send_auto (sock, msg);
	if (err < 0) {
		fprintf (stderr, "failed to send nl message!\n");
	}

out:
	nlmsg_free (msg);
	return err;
}

static int
skip_seq_check (struct nl_msg *msg, void *arg)
{
	return NL_OK;
}

static int
print_rx_msg (struct nl_msg *msg, void *arg)
{
	struct nlattr *attr[GENL_ATTR_MSG + 1];

	genlmsg_parse (nlmsg_hdr (msg), 0, attr, GENL_ATTR_MSG, genl_policy);

	if (!attr[GENL_ATTR_MSG]) {
		fprintf (stdout, "Kernel sent empty message!!\n");
		response.data_ptr = "";
		return -1;
	}

	response.data_ptr = nla_get_string (attr[GENL_ATTR_MSG]);
	return NL_OK;
}

static void
prep_nl_sock (struct nl_sock **nlsock)
{
	int family_id, grp_id;
	unsigned int bit = 0;

	*nlsock = nl_socket_alloc ();
	if (!*nlsock) {
		fprintf (stderr, "Unable to alloc nl socket!\n");
		exit (EXIT_FAILURE);
	}

	/* disable seq checks on multicast sockets */
	nl_socket_disable_seq_check (*nlsock);
	nl_socket_disable_auto_ack (*nlsock);

	/* connect to genl */
	if (genl_connect (*nlsock)) {
		fprintf (stderr, "Unable to connect to genl!\n");
		goto exit_err;
	}

	/* resolve the generic nl family id*/
	family_id = genl_ctrl_resolve (*nlsock, GENL_FAMILY_NAME);
	if (family_id < 0) {
		fprintf (stderr, "Unable to resolve family name!\n");
		goto exit_err;
	}

	if (!mcgroups)
		return;

	while (bit < sizeof (unsigned int)) {
		if (!(mcgroups & (1 << bit)))
			goto next;

		grp_id = genl_ctrl_resolve_grp (*nlsock, GENL_FAMILY_NAME, genl_mcgrp_names[bit]);

		if (grp_id < 0) {
			fprintf (stderr, "Unable to resolve group name for %u!\n", (1 << bit));
			goto exit_err;
		}
		if (nl_socket_add_membership (*nlsock, grp_id)) {
			fprintf (stderr, "Unable to join group %u!\n", (1 << bit));
			goto exit_err;
		}
	next:
		bit++;
	}

	return;

exit_err:
	nl_socket_free (*nlsock); // this call closes the socket as well
	exit (EXIT_FAILURE);
}

static int
my_send (struct payload_data_object *obj)
{
	// printf ("%s start\n", __func__);
	return send_msg_to_kernel (g_nlsock, obj->data_ptr);
}

static int
my_recv (struct payload_data_object *obj)
{
	return nl_recvmsgs (g_nlsock, cb);
}

int
main (int argc, char **argv)
{
	srand ((unsigned int)(time (NULL)));
	struct timer_info *timers	= NULL;
	struct payload_data_object data = { .data_len = MAX_DATA_OBJECT_LEN,
					    .data_ptr = (char *)malloc (MAX_DATA_OBJECT_LEN),
					    .status   = 0 };

	/* init functions */
	parse_args (argc, argv);
	add_group ("0");
	prep_nl_sock (&g_nlsock);
	generate_random_data (&data);

	if (set_schedule (g_schedule_policy, 90, g_process_time, g_deadline_time, g_period_time)
	    != 0) {
		printf ("error setting schedule policy (%d)\n", errno);
		exit (EXIT_FAILURE);
	}
	/* prep callback functions */
	cb = nl_cb_alloc (NL_CB_DEFAULT);
	nl_cb_set (cb, NL_CB_SEQ_CHECK, NL_CB_CUSTOM, skip_seq_check, NULL);
	nl_cb_set (cb, NL_CB_VALID, NL_CB_CUSTOM, print_rx_msg, NULL);

	timers = (struct timer_info *)malloc (sizeof (struct timer_info) * g_test_cycles);

	// nl_recvmsgs (g_nlsock, cb);

	/* start worker */
	worker_thread (&data, timers, my_send, my_recv);

	/* print results */
	print_timers (timers, g_test_cycles);

	/* cleanup */
	nl_cb_put (cb);
	nl_socket_free (g_nlsock);
	free (timers);
	return 0;
}
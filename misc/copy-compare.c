/**
 * @file copy-compare.c
 * @brief compares strncpy, snprintf, memcpy speeds
 *	  hint: memcpy > strncpy > snprintf (2x slow)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LEN 2048

char destination[LEN];
char source[LEN]
	= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt leo eget mi "
	  "suscipit lobortis. Duis hendrerit tempus erat in auctor. Ut pharetra eu felis quis "
	  "tempus. Nam in nibh nulla. Sed ultricies justo ac nibh rutrum, vitae dictum massa "
	  "dapibus. Nunc suscipit maximus gravida. Mauris nec elit eu ex maximus rhoncus quis vel "
	  "lorem. Nunc id dolor vitae metus faucibus efficitur vitae vel turpis. Etiam molestie "
	  "nunc consequat lorem interdum, ac tristique orci congue. Suspendisse sed sem blandit "
	  "nisi venenatis porta eget eu ligula. Vivamus ut imperdiet ante. Vestibulum nisi lorem, "
	  "scelerisque in bibendum vel, iaculis eu leo. Curabitur porttitor enim at iaculis "
	  "rhoncus. Proin ac consequat leo. Proin eleifend a lectus vitae imperdiet. Donec auctor "
	  "blandit ex. Maecenas a tortor ipsum. Maecenas risus lorem, accumsan sed tortor in, "
	  "efficitur pharetra urna. Duis eget lorem sapien. Vivamus id ultrices nisi, quis "
	  "convallis ind nibh. Ut vel faucibus velit. Vivamus nisl nisl, feugiat a finibus mollis, "
	  "tristique quis dolor. Cras nec condimentum neque. Curabitur risus enim, commodo id "
	  "mollis id, pellentesque volutpat eros. Duis iaculis tellus sit amet augue suscipit "
	  "dapibus. Nunc enim sapien, pulvir enim, ullamcorper imperdiet lacus. Fusce eu iaculis "
	  "risus, eget imperdiet eros. Donec eu gravida sapien. Duis pharetra ultrices tellus eget "
	  "luctus. Integer sodales nunc quis nunc fringilla vulputate. Fusce ut nunc sed quam "
	  "bibendum commodo vitae et nibh. Aenean maximus vitae dui vel tincidunt. Fusce faucibus "
	  "efficitur tortor non varius. Phasellus imperdiet eget diam et mollis. Nulla facilisi. "
	  "Phasellus posuere vulputate nunc vitae vestibulum. Pellentesque habitant morbi "
	  "tristique senectus et netus et malesuada fames ac turpis egestas. Donec massa urna, "
	  "mattis quis gravida suscipit, venenatis sit amet enim. Integer interdum tortor justo, "
	  "at fringilla massa faucibus a. Nulla vel est ac nunc eleifend imperdiet. Nunc sagittis "
	  "eleifend nibh, non feugiat purus laoreet ut. Etiam id magna et libero dapibus malesuada "
	  "at sed vel.";

struct timespec start;
struct timespec end;

int
main (void)
{
	size_t count   = 1000 * 1000;
	long delay_sum = 0;

	for (size_t i = 0; i < count; i++) {
		clock_gettime (CLOCK_MONOTONIC, &start);
		strncpy (destination, source, LEN);
		clock_gettime (CLOCK_MONOTONIC, &end);
		delay_sum += end.tv_nsec - start.tv_nsec;
		memset (destination, 0, LEN);
	}

	printf ("strncpy: \t%ld ns\n", (delay_sum / count) );
	delay_sum = 0;

	for (size_t i = 0; i < count; i++) {
		clock_gettime (CLOCK_MONOTONIC, &start);
		snprintf (destination, LEN, "%s", source);
		clock_gettime (CLOCK_MONOTONIC, &end);
		delay_sum += end.tv_nsec - start.tv_nsec;
		memset (destination, 0, LEN);
	}

	printf ("snprintf: \t%ld ns\n", delay_sum / count);
	delay_sum = 0;

	for (size_t i = 0; i < count; i++) {
		clock_gettime (CLOCK_MONOTONIC, &start);
		memcpy (destination, source, LEN);
		clock_gettime (CLOCK_MONOTONIC, &end);
		delay_sum += end.tv_nsec - start.tv_nsec;
		memset (destination, 0, LEN);
	}

	printf ("memcpy: \t%ld ns\n", delay_sum / count);
	delay_sum = 0;
}
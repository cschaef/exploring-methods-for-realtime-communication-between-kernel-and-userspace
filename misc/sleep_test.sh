#!/bin/bash

n_cycles="10000"
sleep_compare="sleep_compare"
policies=( "OTHER" "FIFO" "RR" "DEADLINE" )
script=$(basename $0)
plot_cmd="plot "
plot_output="images/clock_nanosleep_comparision"
#kernel_ver=$(uname -r | sed 's/[_]/\\\\\\_/g')
kernel_ver="4.19.71-rt24-v7l+"

echo "$script running a full test of all policies with $n_cycles cycles each"

echo "$script build binary"
gcc $sleep_compare.c -o $sleep_compare

echo "$script starting stress test for 2minutes"

#sudo stress-ng --matrix 0 -t 2m &
#sleep 5

for policy in "${policies[@]}"
do
	#outputfile="../data/clock_nanosleep_precision_with_$policy.dat"
	 outputfile="../data/data_with_stress/clock_nanosleep_precision_with_$policy.dat"
	# outputfile="../data/data_without_stress/clock_nanosleep_precision_with_$policy.dat"
	echo "$script running test with $policy policy ..."
	#sudo ./$sleep_compare -c $n_cycles -p $policy > $outputfile
	plot_cmd+="\"$outputfile\" w l title \"$policy\", "
	
done


echo "$script stopping stress-ng"
sudo kill -9 $(pidof stress-ng)

echo "$script remove binary"
rm $sleep_compare

echo $script create temporary image dir
mkdir -p images

echo "$script plotting"
gnuplot <<- EOF
        set xrange[0000:900]
        set yrange[490:610]
        set xlabel "Sleep cycles"
        set ytics 0,20
        set ylabel "Time (ns)" 
        set grid
	set termopt 
	set term epslatex color
        set output "${plot_output}.tex"
        ${plot_cmd}
EOF

echo "$script chmod to old user (slowly this gets ugly)"
chown cschaef $plot_output.*

echo "$script copy to images folder"
cp $plot_output.* ../documentation/thesis/images

echo "$script cleanup"
rm $plot_output.*

echo "$script done"

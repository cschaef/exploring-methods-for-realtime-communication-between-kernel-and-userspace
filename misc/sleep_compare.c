/**
 * @file  sleep_compare.c
 * @brief uses clock_nanosleep() to sleep for a time period (500us) and measures with
 * clock_gettime() the actual sleeping time.
 * To use the DEADLINE schedule policy, this program must be run as root, if not the policy will not
 * be applied and run as normal process.
 *
 * @references
 * 	https://man7.org/linux/man-pages/man2/sched_setattr.2.html
 * 	https://www.kernel.org/doc/html/latest/scheduler/sched-deadline.html
 * gnuplot:
 * 	set xrange[4000:6000]
 * 	set yrange[480:640]
 * 	set xlabel "sleep runs"
 * 	set ytics 0,10
 * 	set ylabel "measured time of clock_nanosleep()"
 * 	set grid
 * 	plot 'clock_nanosleep_precision_with_rt_scheduling.dat' w l title "deadline scheduled
 * process", 'clock_nanosleep_precision_without_rt_scheduling.dat' w l title "normal process"
 *  */

#include <errno.h>
#include <getopt.h>
#include <linux/types.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define SCHED_DEADLINE 6
#define PROGRAM_NAME   "sleep_compare"

/* XXX use the proper syscall numbers */
#ifdef __x86_64__
	#define __NR_sched_setattr 314
	#define __NR_sched_getattr 315
#endif

#ifdef __i386__
	#define __NR_sched_setattr 351
	#define __NR_sched_getattr 352
#endif

#ifdef __arm__
	#define __NR_sched_setattr 380
	#define __NR_sched_getattr 381
#endif

struct sched_attr {
	__u32 size;

	__u32 sched_policy;
	__u64 sched_flags;

	/* SCHED_NORMAL, SCHED_BATCH */
	__s32 sched_nice;

	/* SCHED_FIFO, SCHED_RR */
	__u32 sched_priority;

	/* SCHED_DEADLINE (nsec) */
	__u64 sched_runtime;
	__u64 sched_deadline;
	__u64 sched_period;
};

size_t cycle_count = 1000;
int sched_policy   = 0;

int
sched_setattr (pid_t pid, const struct sched_attr *attr, unsigned int flags)
{
	return syscall (__NR_sched_setattr, pid, attr, flags);
}

int
sched_getattr (pid_t pid, struct sched_attr *attr, unsigned int size, unsigned int flags)
{
	return syscall (__NR_sched_getattr, pid, attr, size, flags);
}

void
worker (long sleep_time_us, int N)
{
	struct timespec req = { 0 };
	struct timespec start, end = { 0 };

	for (int i = 0; i < N; i++) {
		clock_gettime (CLOCK_MONOTONIC, &req);

		/* set sleep time, convert sleep (us) to nanoseconds */
		req.tv_nsec += sleep_time_us * 1000;

		/* check for timer overflow */
		if (req.tv_nsec >= 1000000000L) {
			req.tv_nsec -= 1000000000L;
			req.tv_nsec++;
		}

		/* use CLOCK_MONOTONIC, as CLOCK_REALTIME will be depended on the sysclock which can
		 * be changed */
		clock_gettime (CLOCK_MONOTONIC, &start);
		clock_nanosleep (CLOCK_MONOTONIC, TIMER_ABSTIME, &req, NULL);
		clock_gettime (CLOCK_MONOTONIC, &end);

		/* calculate sleep time */
		printf ("%f\n", (float)(end.tv_nsec - start.tv_nsec) / 1000);

		/* yield leftover time to scheduler */
		 sched_yield ();
	}
}

const char *
get_policy ()
{
	if (sched_policy == SCHED_DEADLINE)
		return "SCHED_DEADLINE";
	if (sched_policy == SCHED_FIFO)
		return "FIFO";
	if (sched_policy == SCHED_RR)
		return "RR";
	return "OTHER";
}

void
print_help (void)
{
	printf ("%s Help:", PROGRAM_NAME);
	printf ("  Options\n");
	printf ("    -c <n>        number of cycle to run. default value is 1000\n");
	printf ("    -h            print this page\n");
	printf ("    -p <policy>   scheduler policy. default value is DEADLINE.\n");
	printf ("                  valid options are: FIFO\n");
	printf ("                                     RR\n");
	printf ("                                     DEADLINE\n");
	printf ("                                     OTHER\n");
}

void
parse_args (int argc, char **argv)
{
	int opt = 0;
	while ((opt = getopt (argc, argv, "c:p:h")) != -1) {
		switch (opt) {
		case 'h':
			print_help ();
			exit (EXIT_SUCCESS);
		case 'c':
			cycle_count = atoi (optarg);
			break;
		case 'p':
			if (!strcmp (optarg, "DEADLINE")) {
				sched_policy = SCHED_DEADLINE;
				break;
			} else if (!strcmp (optarg, "RR")) {
				sched_policy = SCHED_RR;
				break;
			} else if (!strcmp (optarg, "FIFO")) {
				sched_policy = SCHED_FIFO;
				break;
			} else {
				sched_policy = SCHED_OTHER;
				break;
			}
		case '?':
			print_help ();
			exit (EXIT_FAILURE);
		}
	}
}

int
main (int argc, char **argv)
{
	int ret		     = 0;
	long cycle_time_us   = 1000;
	long working_time_us = 600;
	long sleeping_time_us = working_time_us - 100;

	parse_args (argc, argv);

	struct sched_attr attr = { .size	   = sizeof (attr),
				   .sched_flags	   = 0,
				   .sched_nice	   = 0,
				   .sched_policy   = sched_policy,
				   .sched_runtime  = working_time_us * 1000,
				   .sched_period   = cycle_time_us * 1000,
				   .sched_deadline = cycle_time_us * 1000 };

	/* set priority seperately for FIFO & RR, as it will lead to errors otherwise */
	if(attr.sched_policy == SCHED_FIFO || attr.sched_policy == SCHED_RR) {
		attr.sched_priority = 90;
	}

	if (getuid () != 0 && errno == 1) {
		printf ("# Program is not executed as root - deadline schedule will not "
			"apply.\n");
	} else {
		printf ("# Program executed with root privileges - deadline schedule will be "
			"applied\n");
	}

	printf ("# sleep-time: %ldus\n", working_time_us);
	printf ("# policy: %s\n", get_policy ());
	ret = sched_setattr (0, &attr, 0);
	if (ret < 0) {
		perror ("# error setting scheduling policy\n");
	}
	worker (sleeping_time_us, cycle_count);
	return 0;
}

#!/bin/bash
methods=( "0_netlink" "1_proc" "2_mmap" "3_sysfs" )
policies=( "OTHER" "FIFO" "RR" "DEADLINE" )
data_directory="data/time_consumption"
script_dir=$(pwd)
cycles=100
testname="request-response"
length=2047

# check if executed with privileges
if [ "$(id -u)" != "0" ]; then
        echo "execute script as root user!"
        exit 1
fi

# get root directory
cd ..
directory_root=$(pwd)

# prepare data dir
directory_data="$directory_root/data/time_consumption"
mkdir -p $directory_data

# run request-response test for each method
for method in "${methods[@]}"
do
	# define temp variables
	directory_method_plots="$directory_data/$method/images"
	gnuplot_generate_cmb_script="gnuplot_generate_cmb.plt"
	gnuplot_generate_seperate_script="gnuplot_generate_seperate.plt"
	tex_image="$testname-$method.tex"
	eps_image="$testname-$method.eps"
	png_image="$testname-$method.png"

	# reset commands, or it will be combined with the previous
	gnuplot_full_command=""
	gnuplot_app_command=""
	gnuplot_kernel_command=""

	# prepare data and plot directory
	mkdir -p $directory_data/$method
	mkdir -p $directory_method_plots
	
	# switch to source directory
	cd $directory_root/$method

	# build
	make clean
	make 
	make app 
	make remove
	make install
	name_lkm="$(echo $method | cut -c 3-)_lkm.ko"
	name_app="$(echo $method | cut -c 3-)_application.out"

	# execute tests
	for policy in "${policies[@]}"
	do
		result_file="$testname-$method-$policy.dat"
		./$name_app -c $cycles -w 800 -d 1000 -p 1000 -l $length -s $policy > $directory_data/$method/$result_file
		gnuplot_full_command+="\"$result_file\" using 1 title \"$policy\", "
		gnuplot_app_command+="\"$result_file\" using 2 title \"$policy\", "
		gnuplot_kernel_command+="\"$result_file\" using 3 title \"$policy\", "
	done

	# trim the last two characters (comma & space)
	gnuplot_full_command=${gnuplot_full_command::-2}
	gnuplot_app_command=${gnuplot_app_command::-2}
	gnuplot_kernel_command=${gnuplot_kernel_command::-2}

	# switch to data directory
	cd $directory_data/$method
	
	# generate png and eps gnuplot scripts
	touch $gnuplot_generate_cmb_script
	cat >$gnuplot_generate_cmb_script << EOL
set key box width 0.75 height 0.75 opaque
set ylabel "request-response time (us)" 
set xlabel "test cycles"
set grid
set termopt 
set term png size 1280, 720
set output "images/combined_${png_image}"
plot ${gnuplot_full_command}
set term epslatex color
set output "images/combined_${tex_image}"
plot ${gnuplot_full_command}
EOL
	
	# generate png and eps gnuplot scripts with kernel and app times split
	touch $gnuplot_generate_seperate_script
	cat >$gnuplot_generate_seperate_script << EOL
set key box width 0.75 height 0.75 opaque
set xlabel "test cycles"
set grid
set termopt 
set term png size 1280, 720
set output "images/seperate_${png_image}"
set multiplot layout 2,1
set ylabel "(app) request time (us)" 
plot ${gnuplot_app_command}
set ylabel "(kernel) response time (us)" 
plot ${gnuplot_kernel_command}
unset multiplot
set term epslatex color
set output "images/seperate_${tex_image}"
set multiplot layout 2,1
set ylabel "(app) request time (us)" 
plot ${gnuplot_app_command}
set ylabel "(kernel) response time (us)" 
plot ${gnuplot_kernel_command}
unset multiplot
EOL
	# check if gnuplot is installed and plot results into images/
	gnuplot_installed=$(gnuplot --version | wc -l 2>/dev/null)
	if [ "$gnuplot_installed" == "1" ]; then
		gnuplot $gnuplot_generate_cmb_script
		gnuplot $gnuplot_generate_seperate_script
	fi

	# change ownership as this script is executed as root 
	current_user="${SUDO_USER:-$USER}"
	chown -R $current_user $directory_data/$method
done
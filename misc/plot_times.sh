#!/bin/bash

methods=( "0_netlink" "1_proc" "2_mmap" "3_sysfs" )

cd ../data/time_consumption

for method in "${methods[@]}"
do
	cd $method
	gnuplot gnuplot_generate_cmb_hist.plt
	#gnuplot gnuplot_generate_cmb.plt
	#gnuplot gnuplot_generate_seperate.plt
	cd ..
done
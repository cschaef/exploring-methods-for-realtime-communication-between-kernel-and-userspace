# Exploring methods for realtime-communication between kernel- and userspace
Bachelor thesis to find the most efficient way to communicate data between the Linux kernel and userspace. In this project the _VARAN_ field bus system from _Sigmatek_ [1] and their _vm111_ kernel module implementation is used. The driver must send and receive data between the bus system and a control application in user space in real time. This work will examine different methods and interfaces for their performance in terms of deterministic speed and reliability, show their advantages and disadvantages and finally give a recommendation.

## Kernel &harr; Userspace Communication Methods
* shared memory
* virtual filesystem (/proc, /sys, /dev)
* syscall
* signaling
* netlink
* ...

## ToDo

## References
[1] https://www.sigmatek-automation.com/en/products/real-time-ethernet-varan

## Timetracker
Meetings - 5h
22.02.21 - 4h
23.02.21 - 1h
24.02.21 - 4h
25.02.21 - 2h
26.02.21 - 4h
27.02.21 - 4h
01.03.21 - 5h
02.03.21 - 5h
03.03.21 - 4h
05.03.21 - 2h
07.03.21 - 4h
08.03.21 - 4h
09.03.21 - 5h
10.03.21 - 1h
11.03.21 - 3h
12.03.21 - 2h
14.03.21 - 8h
15.03.21 - 8h
16.03.21 - 6h
17.03.21 - 6h
18.03.21 - 6h
21.03.21 - 4h
22.03.21 - 3h
23.03.21 - 3h
25.03.21 - 2h
29.03.21 - 1h
31.03.21 - 4h
02.04.21 - 7h
03.04.21 - 2h..
...

## Crash Counter
> 20 (will not be counted anymore ;) )
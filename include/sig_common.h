/**
 * @file sig_common.h
 * @brief contains global definitions and functions
 */

#ifndef SIG_COMMON
#define SIG_COMMON

#ifndef __KERNEL__
	#include <time.h>
	#include <stdlib.h>
	#include <sys/random.h>
	#define u8  __u8
	#define u16 __u16
	#define u32 __u32
	#define u64 __u64
#else
	#include <linux/random.h>
	#include <linux/time.h>
	#include <linux/sched.h>
	#include <linux/types.h>
	#include <uapi/linux/sched/types.h>
#endif

#define MAX_DATA_OBJECT_LEN 2048
#define SCHED_PRIORITY	    90

/* in nanoseconds */
int g_sched_runtime  = 5000;
int g_sched_deadline = 7000;
int g_sched_period   = 8000;

/* ignore pr_fmt redefined error, as its defined in linux/printk.h */
#undef pr_fmt
#define pr_fmt(fmt) "%s:%s: " fmt, KBUILD_MODNAME, __func__

/**
 * @brief data representing object as a linked list
 */
struct payload_data_object {
	u8 status;
	char *data_ptr;
	size_t data_len;
};

void
generate_random_string (char *to, size_t length)
{
	size_t i;
	const char charset[] = "abcdefghijklmnopqrstuvwxyz";
	for (i = 0; i < length; i++) {
#ifdef __KERNEL__
		int rand;
		get_random_bytes (&rand, sizeof (rand));
		to[i] = charset[rand % sizeof (charset) - 1];
#else
		to[i] = charset[rand () % (sizeof (charset) - 1)];
#endif
	}
	to[length - 1] = '\0';
}

/**
 * @brief generates random data for an object
 * @param obj
 */
void
generate_random_data (struct payload_data_object *obj)
{
	if (obj->data_len == 0) {
		obj->data_len = MAX_DATA_OBJECT_LEN;
	}
	generate_random_string (obj->data_ptr, obj->data_len);
}

/**
 * http://www.cse.yorku.ca/~oz/hash.html
 */
u32
hash (const char *str)
{
	unsigned long hash = 5381;
	int c;

	while ((c = *str++))
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

/**
 * @brief
 * @return 0 if successfull, negative otherwise
 */
#ifdef __KERNEL__
int
set_scheduling_policy (void)
{
	#ifdef __arm__
	/*
	struct sched_param param = { .sched_priority = SCHED_PRIORITY };
	return sched_setscheduler (get_current (), SCHED_RR, &param);
	*/
	const struct sched_attr attr = { .sched_policy	 = SCHED_DEADLINE,
					 .sched_flags	 = 0,
					 .sched_nice	 = 0,
					 .sched_priority = 0,
					 .sched_runtime	 = g_sched_runtime,
					 .sched_deadline = g_sched_deadline,
					 .sched_period	 = g_sched_period };
	pr_info ("setting SCHED_DEADLINE params in nsg: runtime: %lld, deadline: %lld, period: "
		 "%lld\n",
		 attr.sched_runtime, attr.sched_deadline, attr.sched_period);

	return sched_setattr (get_current (), &attr);
	#else
	pr_info ("PREEMPT_RT not installed - skip sched_setscheduler()\n");
	return 0;
	#endif
}
#endif
#endif
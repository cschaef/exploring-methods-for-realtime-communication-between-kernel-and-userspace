/**
 * @file app_common.h
 * @brief various common variables and functions used for all testing applications
 *
 */

#ifndef APP_COMMON
#define APP_COMMON

#include <errno.h>
#include <getopt.h>
#include <linux/types.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>


#include "sig_common.h"

#define SCHED_DEADLINE	6
#define MAX_MESSAGE_LEN 64

/* use the proper syscall numbers */
#undef __NR_sched_getattr
#undef __NR_sched_setattr

#ifdef __x86_64__
	#define __NR_sched_setattr 314
	#define __NR_sched_getattr 315
#endif

#ifdef __i386__
	#define __NR_sched_setattr 351
	#define __NR_sched_getattr 352
#endif

#ifdef __arm__
	#define __NR_sched_setattr 380
	#define __NR_sched_getattr 381
#endif

int g_schedule_policy = SCHED_RR;
int g_test_cycles     = 0;
int g_process_time    = 800;
int g_deadline_time   = 1000;
int g_period_time     = 1000;
int g_sleep_time      = 0;
int g_len	      = MAX_DATA_OBJECT_LEN;

struct payload_data_object response;

struct sched_attr {
	__u32 size;

	__u32 sched_policy;
	__u64 sched_flags;

	/* SCHED_NORMAL, SCHED_BATCH */
	__s32 sched_nice;

	/* SCHED_FIFO, SCHED_RR */
	__u32 sched_priority;

	/* SCHED_DEADLINE (nsec) */
	__u64 sched_runtime;
	__u64 sched_deadline;
	__u64 sched_period;
};

struct timer_info {
	struct timespec start;
	struct timespec end;
	struct timespec kernel_start;
	struct timespec kernel_end;
};

int
verify (struct payload_data_object *request, u32 request_hash)
{
	return -(hash (request->data_ptr) != request_hash);
}

/**
 * @brief generic worker routine which passes request and response objects to a sender and receiver
 * functions
 * @param data_list object list
 * @param timers array to save timer differences
 * @param send sender function which takes a list as param to send
 * @param recv receiver function
 */
void
worker_thread (struct payload_data_object *request, struct timer_info *timers,
	       int (*send) (struct payload_data_object *),
	       int (*recv) (struct payload_data_object *))
{
	response.data_len = g_len;
	response.data_ptr = (char *)malloc (g_len);
	response.status	  = 0;

	for (size_t i = 0; i < (size_t)g_test_cycles; i++) {
		struct timer_info timer = { 0 };

		/* start timer */
		clock_gettime (CLOCK_MONOTONIC, &timer.start);

		/* send request downstream */
		send (request);

		/* wait for answer */
		clock_gettime (CLOCK_MONOTONIC, &timer.kernel_start);
		recv (&response);
		clock_gettime (CLOCK_MONOTONIC, &timer.kernel_end);

		/* stop timer */
		clock_gettime (CLOCK_MONOTONIC, &timer.end);

		/* backup timers struct */
		memcpy (timers++, &timer, sizeof (timer));

		/* str compare */
		if (strcmp (request->data_ptr, response.data_ptr) != 0) {
			printf ("string compare failed:\n");
			printf ("    send: %s\n", request->data_ptr);
			printf ("    recv: %s\n", response.data_ptr);
			exit (EXIT_FAILURE);
		}
		if (g_schedule_policy != SCHED_DEADLINE) {
			usleep (g_sleep_time);
		}
		/* yield left over time to scheduler, this period is done */
		sched_yield ();
	}
}

/**
 * @brief prints a timer array to stdout in us
 * @param timer_ns array in nanoseconds
 * @param len size of array
 */
void
print_timers (struct timer_info timers[], size_t len)
{
	float mean_duplex = 0;
	float mean_kernel = 0;
	float mean_app	  = 0;

	printf ("# printing time consumption in microseconds\n");
	printf ("# full    \t app     \t kernel\n");

	for (size_t i = 0; i < len; i++) {
		float duplex = ((float)(timers[i].end.tv_nsec - timers[i].start.tv_nsec)) / 1000.0;
		float kernel
			= ((float)(timers[i].kernel_end.tv_nsec - timers[i].kernel_start.tv_nsec))
			  / 1000.0;
		float userapp = duplex - kernel;

		// skip negative values ..? 
		if(duplex < 0 || kernel < 0 || userapp < 0) {
			continue;
		}

		mean_duplex += duplex;
		mean_kernel += kernel;
		mean_app += userapp;

		printf ("%f\t %f\t %f\n", duplex, userapp, kernel);
	}

	mean_duplex /= len;
	mean_kernel /= len;
	mean_app /= len;

	printf ("# mean times \n");
	printf ("# full    \t app     \t kernel\n");
	printf ("# %f\t %f\t %f \n", mean_duplex, mean_app, mean_kernel);
}

/**
 * @brief converts the policy enum to a string
 * @param policy scheduling algorithm as enum (sched.h)
 * @return policy name as a string
 */
const char *
get_policy (int policy)
{
	if (policy == SCHED_DEADLINE)
		return "SCHED_DEADLINE";
	if (policy == SCHED_FIFO)
		return "FIFO";
	if (policy == SCHED_RR)
		return "RR";
	return "OTHER";
}

/**
 * @brief shows help and possible commandline arguments
 */
void
print_help (void)
{
	printf ("%s help:\n", PROGRAM_NAME);
	printf ("  Options\n");
	printf ("    -c, --cycles <n>          number of cycle to run. default value is 1000\n");
	printf ("    -d, --deadline <n>        time in us for the deadline\n");
	printf ("    -l, --length <n>          set the string len (default %d)\n",
		MAX_DATA_OBJECT_LEN);
	printf ("    -h, --help                print this page\n");
	printf ("    -p, --period <n>          time in us for a single check period\n");
	printf ("    -s, --schedule <option>   set schedule policy. Options are:\n");
	printf ("                                           FIFO\n");
	printf ("                                           RR\n");
	printf ("                                           DEADLINE\n");
	printf ("                                           OTHER\n");
	printf ("    -S, -sleep <us>           set a sleep time in us between cycles\n");
	printf ("    -w, --working_time <n>    time in us to process a roundtrip ipc\n");
	printf ("\n");
	printf ("Example: \n");
	printf ("sudo ./%s.out -c 100 -w 800 -d 1000 -p 1000 -s DEADLINE -l 2048\n", PROGRAM_NAME);
	printf ("sudo ./%s.out -c 100 -s RR -l 64\n", PROGRAM_NAME);
	printf ("\n");
}

/**
 * @brief parses command line arguments and sets global options
 * @param argc
 * @param argv
 */
void
parse_args (int argc, char **argv)
{
	if (argc <= 1) {
		print_help ();
		exit (EXIT_FAILURE);
	}
	int option, option_index = 0;
	static struct option long_options[] = {
		{ "cycles", required_argument, 0, 'c' },  { "deadline", required_argument, 0, 'd' },
		{ "length", required_argument, 0, 'l' },  { "help", no_argument, 0, 'h' },
		{ "period", required_argument, 0, 'p' },  { "schedule", required_argument, 0, 's' },
		{ "sleep", required_argument, 0, 'S' },	  
		{ "working", required_argument, 0, 'w' }, { NULL, 0, 0, 0 }
	};

	while ((option = getopt_long (argc, argv, "c:d:l:ht:p:s:S:w:", long_options, &option_index))
	       != -1) {
		switch (option) {
		case 'c':
			g_test_cycles = atoi (optarg);
			if (g_test_cycles == 0) {
				g_test_cycles = 1000;
			}
			break;
		case 'd':
			g_deadline_time = atoi (optarg);
			if (g_deadline_time == 0) {
				g_deadline_time = 1000;
			}
			break;
		case 'l':
			g_len = atoi (optarg);
			if (g_len == 0) {
				g_len = MAX_DATA_OBJECT_LEN;
			}
			break;
		case 'h':
			print_help ();
			exit (EXIT_FAILURE);

		case 'p':
			g_period_time = atoi (optarg);
			if (g_period_time == 0) {
				g_period_time = 1000;
			}
			break;
		case 's':
			if (!strcmp (optarg, "DEADLINE")) {
				g_schedule_policy = SCHED_DEADLINE;
				break;
			} else if (!strcmp (optarg, "RR")) {
				g_schedule_policy = SCHED_RR;
				break;
			} else if (!strcmp (optarg, "FIFO")) {
				g_schedule_policy = SCHED_FIFO;
				break;
			} else {
				g_schedule_policy = SCHED_OTHER;
				break;
			}
		case 'S':
			g_sleep_time = atoi (optarg);
			break;
		case 'w':
			g_process_time = atoi (optarg);
			if (g_process_time == 0) {
				g_process_time = 800;
			}
			break;
		case '?':
		default:
			print_help ();
			exit (EXIT_FAILURE);
		}
	}
}

/**
 * @brief helper function to set a scheduling policy of a process
 */
int
sched_setattr (pid_t pid, const struct sched_attr *attr, unsigned int flags)
{
	return syscall (__NR_sched_setattr, pid, attr, flags);
}

/**
 * @brief helper function to get a scheduling policy of a process
 */
int
sched_getattr (pid_t pid, struct sched_attr *attr, unsigned int size, unsigned int flags)
{
	return syscall (__NR_sched_getattr, pid, attr, size, flags);
}

/**
 * @brief sets a schedule policy and task priority for a thread, in case of SCHED_DEADLINE: runtime
 * < deadline < period
 * @param schedule_policy SCHED_* policy
 * @param schedule_priority set the priority for a task (only in combination with SCHED_FIFO,
 * SCHED_RR)
 * @param schedule_runtime execution time of the task
 * @param schedule_deadline timespan the task has to be finished
 * @param schedule_period  cycle time of the task
 * @return 0 if successfull
 *        -1 otherwise, sets errno
 */
int
set_schedule (__u32 schedule_policy, __u32 schedule_priority, __u64 schedule_runtime,
	      __u64 schedule_deadline, __u64 schedule_period)
{
	struct sched_attr attr = { .size	   = sizeof (attr),
				   .sched_flags	   = 0,
				   .sched_nice	   = 0,
				   .sched_policy   = schedule_policy,
				   .sched_runtime  = schedule_runtime * 1000,
				   .sched_period   = schedule_period * 1000,
				   .sched_deadline = schedule_deadline * 1000 };

	/* set priority seperately for FIFO & RR, as it will lead to errors otherwise */
	if (attr.sched_policy == SCHED_FIFO || attr.sched_policy == SCHED_RR) {
		attr.sched_priority = schedule_priority;
	} else if (attr.sched_policy == SCHED_DEADLINE) {
		/* check deadline periods */
		if (!(schedule_runtime <= schedule_deadline
		      && schedule_deadline <= schedule_period)) {
			errno = -EINVAL;
			return -1;
		}
	}

	if (getuid ()) {
		printf ("# Program is not executed as root - schedule will not be applied.\n");
	} else {
		printf ("# Program started with root privileges - schedule will be applied\n");
	}

	printf ("# sleep-time: %lld ns\n", attr.sched_period);
	printf ("# policy: %s\n", get_policy (attr.sched_policy));

	if (sched_setattr (0, &attr, 0)) {
		printf ("debug\n");
		return -1;
	}
	return 0;
}

#endif
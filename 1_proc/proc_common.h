#ifndef PROC_COMMON
#define PROC_COMMON

#define MAX_BUFFER_LEN 128
#define PROC_FILE      "/proc/proc_lkm"
#define MODULE_NAME    "proc_lkm"
#define PERMISSIONS    0666
#define SCHED_PRIORITY 90

#endif
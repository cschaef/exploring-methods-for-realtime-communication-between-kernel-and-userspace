# Shared Memory IPC

## Preperations
```
sudo dnf install kernel-devel kernel-headers	# fedora
sudo apt install raspberrypi-kernel-headers 	# raspbian
```

## Build
```
$ make 			# Build kernel module
$ make app		# Build userspace application
$ make run		# Build kernel module, install and print dmesg
$ sudo make install	# Install kernel module 
$ sudo make Remove	# Remove kernel module
```

## References
https://developer.ibm.com/technologies/linux/articles/l-kernel-memory-access/  
https://devarea.com/linux-kernel-development-creating-a-proc-file-and-interfacing-with-user-space/  
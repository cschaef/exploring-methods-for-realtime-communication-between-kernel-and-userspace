/**
 * @file	generic_kernel_module.c
 * @brief	kernel module which uses the /proc interface to communicate with userspace.
 * @references
 * 	https://lkw.readthedocs.io/en/latest/doc/05_proc_interface.html
 *	https://devarea.com/linux-kernel-development-creating-a-proc-file-and-interfacing-with-user-space/
 */

#include "proc_common.h"
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/proc_fs.h> /* structs proc_dir_entry, proc_ops */
#include <linux/sched.h>
#include <linux/slab.h>	   /* kmalloc */
#include <linux/uaccess.h> /* copy_to_user(), copy_from_user() */
#include <linux/version.h> /* get kernel version */
#include <uapi/linux/sched/types.h>

#include "../include/sig_common.h"

#define pr_fmt(fmt) "%s:%s: " fmt, KBUILD_MODNAME, __func__

/* data buffer of this kernel module*/
static char *procfs_buffer = NULL;
static struct proc_dir_entry *ent;

/* data buffer length of this kernel module */
size_t data_len;

/**
 * @brief echos a message back to the procfs buffer
 * @param incoming message to handle
 * @param incoming_len length of the message
 */
void
message_handler (const char *incoming, size_t incoming_len)
{
	strncpy (procfs_buffer, incoming, MAX_DATA_OBJECT_LEN);
}

/**
 * Write handler routine, gets called whenever a process writes to the /proc entry
 * @param file ptr to the /proc entry
 * @param buff ptr to the data to be written
 * @param count
 * @param offp
 * @return length of the written data
 */
static ssize_t
proc_write (struct file *file, const char __user *buff, size_t count, loff_t *offp)
{
	char *data = NULL;

	pr_debug ("write handler called\n");


	/* check written message length */
	if (count > MAX_DATA_OBJECT_LEN) {
		pr_err ("message too long\n");
		return -EFAULT;
	}

	/* set a ptr to the /proc file */
	// data = PDE_DATA (file_inode (file));

	/* copy to /proc/entry from userspace */
	// snprintf(procfs_buffer, count, "%s", buff);
	memcpy(procfs_buffer, buff, count);

	/*
	if (copy_from_user (data, buff, count)) {
		pr_err ("failure copying data from /proc entry\n");
		return -EFAULT;
	}

	*/

	/* react on message */
	// message_handler (data, count);

	/* print received message and correct offset ptr and buffer len */
	*offp	 = (int)count;
	data_len = count - 1;

	return count;
}

/**
 * Read handler routine, gets called whenever the /proc entry is read from userspace
 * @param file pointer to the /proc entry
 * @param buff buffer to be passed to the user
 * @param count length of the buffer
 * @param offp
 * @return length of the read data
 */
static ssize_t
proc_read (struct file *file, char __user *buff, size_t count, loff_t *offp)
{
	char *data = NULL;

	pr_debug ("read handler called\n");

	/* ??  */
	if ((int)(*offp) > data_len) {
		return 0;
	}

	/* get file content  */
	// data = PDE_DATA (file_inode (file));

	/* check if there is anything to read in the file */
	/*
	if (data == NULL) {
		pr_warn ("read data is null\n");
		return ENOMSG;
	}
	*/
	if (count == 0) {
		pr_warn ("nothing to read\n");
		return count;
	}
	count = data_len + 1; /* add \0 character */

	/* copy content to userspace */
	memcpy(buff, procfs_buffer, MAX_DATA_OBJECT_LEN);
	// sprintf(buff, "%s", procfs_buffer);

	/*
	if (copy_to_user (buff, procfs_buffer, count)) {
		pr_err ("error while copying data to userspace");
		return EFAULT;
	}
	*/
	*offp = count;
	return count;
}

/* with kernel version 5.6.0 proc_create() uses a proc_ops struct instead of file_operations */
#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 6, 0)
static struct file_operations fops = { .write = proc_write, .read = proc_read };
#else
static const struct proc_ops fops = { .proc_read = proc_read, .proc_write = proc_write };
#endif

/**
 * Creates a /proc entry, allocates memory space and sets a welcome message
 * @return 0 if successfull
 */
int
create_proc_entry (void)
{
	char *starter_msg = "Hello, World";

	/* set initial data length */
	data_len = strlen (starter_msg);

	procfs_buffer = kmalloc (MAX_DATA_OBJECT_LEN, GFP_ATOMIC);
	if (procfs_buffer == NULL) {
		pr_err ("could not allocate memory for data buffer\n");
		return ENOMEM;
	}

	/* copy starter message to buffer */
	strncpy (procfs_buffer, starter_msg, data_len);

	/* creates /proc directory entry */
	ent = proc_create_data (MODULE_NAME, PERMISSIONS, NULL, &fops, procfs_buffer);
	if (ent == NULL) {
		pr_err ("could not create /proc file\n");
		return -1;
	}
	return 0;
}

/**
 * Entry routine for the kernel module, calls create_proc_entry()
 * @return
 */
static int __init
run (void)
{
	pr_info ("Start\n");
	if (create_proc_entry ()) {
		return -1;
	}
	if (set_scheduling_policy () < 0) {
		pr_err ("cannot set scheduling policy\n");
	}
	return 0;
}

/**
 * Exit routine, called when module is removed
 */
static void __exit
cleanup (void)
{
	pr_info ("Release\n");
	proc_remove (ent);
}

module_init (run);
module_exit (cleanup);

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("Christian Schaefer <cschaefer.itsb-b2018@fh-salzburg.ac.at>");

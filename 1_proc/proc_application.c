/**
 * @file 	app.c
 * @brief	userspace app which reads from a /proc file, writes data to it and reads again
 */

#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define PROGRAM_NAME "proc_application"

#include "../include/app_common.h"
#include "../include/sig_common.h"
#include "proc_common.h"

int fd = 0;

int
my_recv (struct payload_data_object *obj)
{
	/* reset fd to the start */
	lseek (fd, 0, SEEK_SET);

	/* read from file */
	return read (fd, obj->data_ptr, obj->data_len);
}

int
my_send (struct payload_data_object *obj)
{
	/* reset fd to the start */
	lseek (fd, 0, SEEK_SET);

	/* write to proc file */
	return write (fd, obj->data_ptr, obj->data_len);
}

int
main (int argc, char **argv)
{
	struct timer_info *timers	= NULL;
	struct payload_data_object data = { .data_len = MAX_DATA_OBJECT_LEN,
					    .data_ptr = (char *)malloc (MAX_DATA_OBJECT_LEN),
					    .status   = 0 };

	generate_random_data (&data);
	fd = open (PROC_FILE, O_RDWR);

	/* init functions */
	parse_args (argc, argv);
	if (set_schedule (g_schedule_policy, 90, g_process_time, g_deadline_time, g_period_time)
	    != 0) {
		printf ("error setting schedule policy (%d)\n", errno);
		exit (EXIT_FAILURE);
	}
	timers = (struct timer_info *)malloc (sizeof (struct timer_info) * g_test_cycles);

	/* start worker */
	worker_thread (&data, timers, my_send, my_recv);

	/* print results and clean up */
	print_timers (timers, g_test_cycles);
	free (timers);
	close (fd);
	return 0;
}
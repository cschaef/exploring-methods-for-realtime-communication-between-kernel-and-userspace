# Writing Ideas:
	* Kernel and Userspace, differences, applications
	* Comparision Table 

# Methods:
	* D-BUS IPC for Kernel: kdbus & Bus1?
	* Linux System Calls (Syscall) (!)
	* Virtual System Call 
	* Virtual Filesystems
	* Signals
	* Shared Memory, Sempahore and Queues (!)
	* IPC Sockets 
	* Sysfs
	* Netlink (!)
	* Procfs 
	* Seq_file
	* Relayfs (!)


# Stress test:
* stress-ng suite


# Real Time requirements of VARAN
Period cycle times defined in vm111.c?

|---- RT task (1000 us) ----|---- RT task (1000 us) ----|---- RT task (1000 us) ----
      800us          |---- iso Task (1000us) ----|---- iso Task (1000us) ----|


Payload:
struct vm111_payload_header_do ... ?


Fragen:
Shadow Buffer? temp speicher für spi kommunikation? --> wird der RB zyklisch abgearbeitet? Welcher Task macht das?


Welche Daten müssen an den Userspace geschickt werden (vm111_payload_header_do / vm111_payload_write_do / vm111_payload_read_do)? Maximale Size von vm111_payload_write_do und vm111_payload_read_do?


RT_TASK läuft als Interrupt Routine, lockt iso_mutex, wird aufgerufen wenn input gpio HIGH wird


Isochronous Task ... was macht der?

handle_irq() 


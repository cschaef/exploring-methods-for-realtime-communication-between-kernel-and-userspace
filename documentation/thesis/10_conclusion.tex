% ===================================================================== %
% Author	Christian Schaefer					%
% Version	0.1							%
% Date		01.05.2021						%
% File		10_conclusion.tex					%
% ===================================================================== %

\section{Conclusion}

In this bachelor thesis, different methods were examined as a communication medium between a Linux kernel module and a userspace programm.

The goal was to find a suitable solution for the \textit{Sigmatek} kernel driver \textit{vm111} for high-performance real-time communication. The driver interacts with a \textit{VARAN} fieldbus master device and therefor it must be ensured that the overlying software does not represent a bottleneck.

In the Linux world there are many possibilities for inter-process communication, but not all of them are suited for this scenario. The first condition is based on the fact that not only commands are exchanged between the two sides, but also data, which can be up to 2048 bytes in size. The second factor to keep in mind is that it is not a simple IPC between two userspace processes, but between a program and a driver located in kernelspace. The third and most important  is that of real time, which means that it is not only enough that data just "arrives", but that it also \emph{guaranteed} that data arrives reliably in a certain amount of time (100 microseconds). This means that it must be ensured that there are no potential interruptions.

With these conditions, the following potential methods were selected: \textit{netlink}, \textit{procfs}, \textit{sysfs}, \textit{shared memory}. In the course of this work, the methods were investigated, their advantages and disadvantages were discussed and their reliability and performance were examined. It turned out that, except for \textit{netlink}, basically any other method would be performant enough. There are, however, following advantages and disadvantages:

\begin{itemize}
	\item \textbf{procfs}: similar to sysfs, best performance, but data must be stored in a suitable format in the file and parsed first when used.
	\item \textbf{sysfs}: slowest method, but would offer a good and elegant solution to represent the data, not ideal when accessing multiple files, sequential \verb|open(), write(), read()| syscalls slow down the process immensely.
	\item \textbf{shared memory}: through zero-copy mode, it is not necessary to work on files, but two processes can work on the same memory at the same time. Here, however, more attention must be paid to appropriate synchronisation and memory access management. Additionally this method is also much more error-prone and can cause system instability due to incorrect memory access on the kernel side.
\end{itemize}

Finally, the question arises as to which method is most suitable. Objectively, data exchange via \verb|/proc| is the most performant and secure.
However, it may be necessary to re-evaluate what data, for what purpose, and at what intervals really need to be sent to a userspace programm and whether it is not possible to reduce the data exchange and realize more data handling in the kernel module.
In addition, care must be taken to implement a suitable mechanism so that both sides (kernel module and application) can work synchronised (see \ref{further_work}).

In conclusion, it can be said that working with real-time is considerably more complex and difficult and requires a different approach. There is no "almost", "quite" or "average" in a real-time context and no aspect can be left to chance. With that in mind, this work shows, there is no one solution that is one hundred percent satisfactory, there are compromises to be made with every method. However, maybe not one method is the solution to all problems, but a good combination of several.
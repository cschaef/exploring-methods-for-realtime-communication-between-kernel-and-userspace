% ===================================================================== %
% Author	Christian Schaefer					%
% Version	0.1							%
% Date		29.04.2021						%
% File		9_analysis_and_results.tex				%
% ===================================================================== %

\section{Analysis and results}
The following chapter contains a summary of the simulation results and should serve as an orientation for future work. In the subsection \ref{further_work} additional points are discussed, which were encountered in the course of this work and need further analysis.

\subsection{Comparison}
The table below shows the comparison of the time differences of the request-response test runs. As can be seen from the previous figures, the average values (apart from Netlink) and the distribution (see the respective histograms) perform very well. Unfortunately, these values do not apply to real-time applications, since the worst cases must be considered here.

\begin{table}[h!]
	\centering
	\begin{tabular}{@{}lllllll@{}}
		        & \multicolumn{2}{l}{SCHED\_DEADLINE} & \multicolumn{2}{l}{SCHED\_RR} & \multicolumn{2}{l}{SCHED\_FIFO}                                      \\\cmidrule(lr){2-3}\cmidrule(lr){4-5}\cmidrule(lr){6-7}
		        & max                                 & average                       & max                             & average   & max        & average   \\ \midrule
		netlink & 302.72                          & 90.91                     & 154.77                      & 89.80 & 151.70 & 89.94 \\
		sysfs   & 72.86                           & 36.11                     & 74.10                       & 35.69 & 76.16  & 35.64 \\
		procfs  & 17.75                           & 08.39                     & 28.01                       & 08.14 & 22.05  & 08.20 \\
		mmap    & 22.94                           & 11.04                     & 26.37                       & 10.90 & 31.35  & 10.97 \\ \bottomrule
	\end{tabular}
	\caption{Request-Response sequence times (us) using different scheduling algorithms\textsuperscript{1} }
\end{table}

\footnotetext[1]{In this table, the scheduling policy SCHED\_OTHER was intentionally omitted, as it is not suitable for applications running in a real-time context.}

Besides the temporal differences, it is also necessary to discuss the different characteristics of the methods and their suitability for the \textit{vm111} driver. As can be seen in table \ref{tab:vm111_payload_header_do_struct}, the data object header consists of many members, some of unknown size.
If one decides to use the \textit{shared memory} method in this case, it would be possible to reserve memory areas dedicated to the storage of structs and to reference the same object address via pointers. In this case, access control is mandatory to avoid data corruption.
If data is exchanged via a file system, the same procedure is possible if no other format is to be used for storage. However, it is important to note that the direct access method via storage areas is risky and prone to more errors.

In contrast, an adequate filesystem structure with sysfs would be a good way to represent data objects. An example structure would look like the one shown in Listing \ref{sysfs_structure} (this approach has already been partly implemented by \textit{Sigmatek} for testing purposes):

\begin{lstlisting}[language=, label=sysfs_structure, caption=sysfs dataobject representation]
/sys/kernel/vm111/
|-- export              -- direct access task
|-- start
|-- object0             -- first object
|   |-- access_t        -- access type: mem_wr, mem_rd, cntrl_wr, cntrl_rd
|   |-- address         -- address
|   |-- cycle_cnt       -- current rt-cycle (updates after DO execution)
|   |-- data            -- containing data
|   |-- option          -- options: required, on, off
|   `-- status          -- state informations
|       |-- error       -- error state (DWORD)
|       `-- retries     -- retry counter
`-- object1             -- next object in list
    [...]
\end{lstlisting}

But as described in chapter \ref{sysfs_detailed}, data exchange via \verb|/sys| becomes impractical if multiple files are written sequentially, as the 100 microsecond limit is quickly exceeded.

One consideration would be to combine the performance of \textit{shared memory} and file structure of \verb|/sys|. Since the method \verb|mmap()| basically maps a file into the virtual address space, the exchange of critical data could be handled by a zero-copy method, while other information like cycle counter, access attributes or alternative options is read and written via the virtual filesystem.
The previous example which uses  shared-memory creates a file in the \verb|/proc| directory and waits for a \verb|mmap()| call from userspace. The same principle could be used with \verb|/sys|.

\subsection{Further work} \label{further_work}
Before mentioned methods are to be implemented in a productive environment, it is recommended that a detailed analysis and simulations are carried out again explicitly for the use case and, if necessary, adapt the real-time requirements.
Regardless of the method used for data exchange, attention should be paid to synchronization in order to guarantee that both applications (user app and kernel driver) are synchronously coordinated.
In order to avoid possible race conditions, it must also be ensured that operations which read or write data always run atomically. Furthermore, a suitable locking mechanism must not be forgotten in the case of shared resources.


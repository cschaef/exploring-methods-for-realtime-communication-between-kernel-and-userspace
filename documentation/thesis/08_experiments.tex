% ===================================================================== %
% Author	Christian Schaefer					%
% Version	0.1							%
% Date		02.03.2021						%
% File		8_experiments.tex					%
% ===================================================================== %

\section{Experiments}
The following sections describe the setup, the procedure and the results of the experiments with the respective methods. The first series of tests will roughly revolve around the basic suitability of different methods. Possible candidates will be examined in more detail in a second series of tests to consider the advantages and disadvantages.

\subsection{Setup and testing requirements}
All measurements were carried out on a \textit{Raspberry Pi}, Model 4 with 2 gigabytes of RAM.
The Raspberry Pi was provided by the FH Salzburg, the image and hardware header tools are supplied by \textit{Sigmatek}. As can be seen in Listing \ref{rpi_image}, a \textit{Linux} kernel with version 4.19.71 and the \textit{PREEMPT\_RT} patch are pre-installed on the image file.

\begin{lstlisting}[language=bash, label=rpi_image, caption=System information output, escapechar=\%]
pi@raspberrypi:~ $ uname -a
Linux raspberrypi 4.19.71-rt24-v7l+ %\#% 1 SMP PREEMPT RT Thu Sep 24 09:56:50 BST 2020 armv7l GNU/Linux
\end{lstlisting}

The upcoming measurements show the time required for a simple request-response (rr) process. A userspace program initiates communication and sends a request to the respective \textit{Linux} kernel module. The module should respond within its next cycle. The parameters for the experiments listed in \ref{sigmatek_requirements} were defined in consultation with Sigmatek. For the following tests, the most relevant parameter is the maximum duration ($rr_t$) of a full request-response process. This consists of the downstream time $d_t$ (userapp $\rightarrow$ kernel module), the data processing time $p_t$ and the upstream time $u_t$ (kernel module $\rightarrow$ userapp).

\begin{figure}[!h]
	\centering
	\begin{align}\label{sigmatek_requirements}
		\text{test cycles } n_t &= 10.000 \\
		\text{period cycle time } c_t &= 1000\:us \\
		\text{max. payload size } d_{s} &= 2048\:Byte \\
		rr_{t} &= d_{t} + u_{t} + p_{t} \\
		rr_{t} &\leq 100\:us
	\end{align}
\end{figure}

In order to achieve the most accurate timestamps possible, the function \verb|clock_gettime()| is used in the implementations. Using the parameter \verb|CLOCK_MONOTONIC| the timestamp equals the passed time since the machine boot, which will not be affected by discontinuous jumps in the system time \cite{man_clock_gettime}.
In the test implementations, four timestamps are taken to measure both the overall procedure and the respond time of the kernel module. To better understand the test sequence, a visual representation is shown in figure \ref{fig:measurement_sequence_diagram} as a sequence diagram.

\newpage
\subsection{Measurements Request-Response Sequence}
The next chapters present the timing results of a request-response action with the respective method. All implementations of the experiments follow the sequence according to figure \ref{fig:measurement_sequence_diagram}. In the first steps, basic tests are performed to clarify whether the respective method is suitable for the use case at all and if the desired real-time conditions can be achieved.


\begin{figure}[!h]
	\centering
	\footnotesize
	\begin{BVerbatim}
   +---------------+                    +---------------+
   | Userspace-App |                    | Kernel-Module |
   +---------------+                    +---------------+
          |                                    |
          | start timer t_1                    |
          |-----------+                        |
          |           |                        |
          |<----------+                        |
          |                                    |
          | send communication request         |
          |----------------------------------->|
          |                                    |
          | start timer t_2                    |
          |--------------+                     |
          |              |                     |
          |<-------------+                     |
          |                      send response |
          |<-----------------------------------|
          |                                    |
          | stop timer t_1, t_2                |
          |-----------+                        |
          |           |                        |
          |<----------+                        |
          |                                    |
	\end{BVerbatim}
	\caption{Measurement sequence diagram }
	\label{fig:measurement_sequence_diagram}
\end{figure}

\subsubsection{Netlink}
The \textit{Netlink Protocol Library Suite} (libnl) offers a versatile application programming interface (API), which makes the implementation of an IPC channel via Netlink much easier. For the thesis example, the basic functions of \textit{libnl} are used and included in the implementation of both the kernel module and the userspace application.

With the help of Netlink, an IPC socket is opened on both sides. The userspace application can send data directly to the remote peer. However, the response from the kernel module is not sent as a unicast, but as a multicast to a Netlink family with which the userspace programme has previously registered. This can be used to simply inform multiple applications of any updates.

\begin{figure}[!h]
	\centering
	\includegraphics{images/combined_request-response-0_netlink.pdf}
	% \input{images/combined_request-response-0_netlink.pdf}
	\caption{netlink request-response times on the Raspberry Pi}
	\label{fig:netlink_rr_full}
\end{figure}

Figure \ref{fig:netlink_rr_full} shows the measured processing times of the request-response procedure. For this basic test, data in the form of a string with 2048 random bytes is send up- and downstream.
As can be seen from the figure, the maximum period time of 100 microseconds is exceeded with all scheduling algorithms.

\begin{comment}
For a detailed analyis an additional timestamp is taken to measure the processing time of the kernel (timer $t_2$ - see sequence diagram in figure \ref{fig:measurement_sequence_diagram}). To better view the difference, the response time of the kernel module, as well as the processing and sending time of the userspace app, are seperate shown in figure \ref{fig:netlink_rr_sperate}. Here again, two things are apparent, the scheduling algorithm has little effect and the request time from userspace takes many multiples of that of the response.

\begin{figure}[!h]
	\centering
	\input{images/seperate_request-response-0_netlink.tex}
	\caption{detailed view of netlink request-response time}
	\label{fig:netlink_rr_sperate}
\end{figure}
\fi
\end{comment}

In conlusion the request and response times are beyond the conditions set at the beginning (see \ref{sigmatek_requirements}). This renders this method as unusable and will not be analyzed further.

\subsubsection{procfs}
The following test examines the performance and suitability of data exchange via the pseudo-filesystem \verb|/proc|. As explained in section \ref{procfs_overview}, the directory \verb|/proc| contains interfaces to kernel modules and internal data structures.
For this research, a kernel module was developed that provides an interface for userspace communication with the file \verb|/proc/proc_lkm|. It is now possible to read and write to this special file with various userspace programs. The kernel module has implemented internal callback functions which react accordingly to any such access.

For the test procedure, a timestamp is taken and the request is initiated as before from the user space by a program writing a string with a length of 2048 Bytes to \verb|/proc/proc_lkm|. Upon receiving the answer from the kernel module a second timestamp is taken. The results are shown in figure \ref{fig:procfs_rr_full}.

\begin{figure}[!h]
	\centering
	\includegraphics{images/combined_request-response-1_proc.pdf}
	\caption{procfs request-response times on the Raspberry Pi}
	\label{fig:procfs_rr_full}
\end{figure}

Compared to \textit{netlink}, using \textit{procfs} results in a significant increase in performance. As can be seen it is clear that the scheduling policy \verb|SCHED_OTHER| definitly takes the most time. Besides that, using other policies it is easy to stay within the 100 microseconds condition. As this method is certainly feasible regarding its performance, it is worth analysing further.

\begin{comment}

\begin{figure}[!h]
	\centering
	\input{images/seperate_request-response-1_proc.tex}
	\caption{detailed view of procfs request-response time}
	\label{fig:procfs_rr_sperate}
\end{figure}

Figure \ref{fig:procfs_rr_sperate} shows a more detailed view of the measured durations: processing in userspace takes on average about 6 microseconds, the kernel module reacts somewhat slower with an average response time of 6 microseconds. Compared to the \textit{netlink} method this way is much faster and is worth analyising more in detail.
\end{comment}


\subsubsection{mmap}
The next tests cover the exchange of data via shared memory. As explained earlier, this method provides a common memory region that can be used by two or more applications. It is important to maintain control over access, otherwise inconsistencies may occur.

In this example implementation, the shared region is created using the function \verb|mmap()|. This maps the contents of a file into memory and makes it easily accessible via a file descriptor.
The function \verb|mmap()| can also be called with \verb|MAP_ANONYMOUS| as parameter flag, so the shared memory is not backed up by any file and the area is set to zero \cite{man_mmap_2}. However in order to use the memory by multiple processes a pointer must be shared between them. To avoid that a file descriptor to a known file (\verb|/proc/mmap_lkm| was used.
Any operation performed on that file descriptor by a userspace program can be recognized via various callback functions in the kernel module. To prevent multiple processes from accessing the same data at the same time, a temporary mutex is set in the kernel module.
Since the \verb|write()| callback function can also detect when a write access takes place, it is possible to respond to a possible userspace request in the kernel module. As before, timestamps are taken before and after the execution. The resulting measurements can be seen in figure \ref{fig:mmap_rr_comb}.

\begin{figure}[!h]
	\centering
	\includegraphics{images/combined_request-response-2_mmap.pdf}
	\caption{mmap request-response times on the Raspberry Pi}
	\label{fig:mmap_rr_comb}
\end{figure}

Again, it is clear that the worst case times with the scheduling policy \verb|SCHED_OTHER| are the slowest. On a side note it is worth mentioning that all tests, besides any policies, are within 60 microseconds, which means this method is another candidate worth examining in detail. However since the implementation is by far the most complex and error-prone compared to the other methods, it must be weighed up whether this method is the most suitable solely based on the time gain of a few microseconds.

\begin{comment}

\begin{figure}[!h]
	\centering
	\input{images/seperate_request-response-2_mmap.tex}
	\caption{detailed view of mmap request-response time}
	\label{fig:mmap_rr_seperate}
\end{figure}

If one looks at the detailed evaluation, it can be seen that the response of the kernel module is somewhat slower than the write process of the user space program. Since it is possible to exchange data much faster using shared memory, this method will also be considered in further analysis.

\end{comment}

\subsubsection{sysfs}
In the last experiment of this test series, the implementation of the request-response sequence via \verb|sysfs| is tested. Since the principle of data exchange is similar to \textit{procfs} (open, read \& write file), the results of these first tests should be similar. As mentioned above, a major advantage would be that \verb|sysfs| allows a very good representation of a potential data object structure via its hierarchical file system. Since this test is still working with random data and the advantage is therefore not obvious, it will be discussed in more detail in a later chapter.

\begin{figure}[!h]
	\centering
	\includegraphics{images/combined_request-response-3_sysfs.pdf}
	\caption{sysfs request-response times on the Raspberry Pi}
	\label{fig:sysfs_rr_comb}
\end{figure}
Just as before, random data with a length of 2048 bytes is written to a file, the kernel module receives the data and writes it back to the same file. The test is finished as soon as the userspace application receives the data. The results are shown in figure \ref{fig:sysfs_rr_comb}.

Although the principle of reading and writing a file is similar to using the \textit{procfs} method, it is clearly visible, that using \textit{sysfs} has a much wider result spectrum, with some results even exceeding 50 microseconds. It is interesting that even using the policy \verb|SCHED_DEADLINE| the jitter is relative big. Only using the policy \verb|SCHED_OTHER| performs worse with results near to 150 microseconds.
\begin{comment}

\begin{figure}[!h]
	\centering
	\input{images/seperate_request-response-3_sysfs.tex}
	\caption{detailed view of sysfs request-response time}
	\label{fig:sysfs_rr_seperate}
\end{figure}

 In figure \ref{fig:sysfs_rr_seperate} the times of the write (request) and read (response) processes are shown separately (again it is visible that the \verb|SCHED_DEADLINE| scheduling policy takes the most time and has the worst jitter). As there are more than 80 microseconds left for potential application tasks, this method will also be analysed further later on.
\end{comment}


\subsection{Detailed analysis}
With the above results, the first series of tests is now complete. Of the four potential methods, two have emerged that are worth analysing in more detail in the next chapters:

\begin{itemize}
	\item shared memory
	\item sysfs
\end{itemize}

The others methods are no longer considered for detailed analysis. Data communication via \textit{Netlink} was excluded, since the establishment and handling of the IPC sockets takes the longest and has too much jitter. Another disadvantage is the parsing of the received data, a suitable format for the data representation is needed. Data must be processed according to this format, which would involve additional effort and potential sources of error.

The second method which will be ignored for now is the data exchange via \textit{procfs}. Although the read and write operations are very fast and efficient, the implementation via the process file system is unsuitable compared to \verb|/sys|, which has the added advantage of built-in multiple file support and logical representation of a data object struct. Additionally this method would also involve the additional parsing of the data; the detailed analysis will be skipped for now, but it can be also considered as a valid method as well, since its jitter and maximum delay is the best of all mentioned methods.

\subsubsection{sysfs} \label{sysfs_detailed}

\paragraphlb{Writing large amounts of data into one file vs. small amounts of data into many files:}
As already mentioned \verb|sysfs| offers the possibility to create a large file structure hierachy, the question arises now to what extent writing large amounts of data (2048 bytes) into a common file is more performant than writing small amounts of data (16 bytes) into many different files. Basically, it can be assumed that the latter takes longer, because all the more \verb|open()| and \verb|close()| calls are issued.
Figure \ref{fig:buffer_test_static} below shows the duration of a request-response process with different data sizes.

\begin{figure}[!h]
	\centering
	\includegraphics{images/buffer_test_static.pdf}
	\caption{Operating on a single file vs. multiple}
	\label{fig:buffer_test_static}
\end{figure}

In the first part of the test, a random string with 2048 characters was written to a file. After that, the amount of data was split figuratively and 16 bytes were written to 128 different files. As can be seen, in the worst case, the time duration of \emph{one} request-response sequence is close to 45 microseconds. Since write and read processes must always be executed sequentially, the times of the RR cycles must be summed up. If only two worst cases occur during data exchange, the maximum permitted time span (100us) is almost reached. This fact would render the method via \verb|sysfs| obsolete.

In the second figure \ref{fig:buffer_test_dynamic} the data size was increased continuously throughout 1024 test runs to exclude possible unforeseen changes.

\begin{figure}[!h]
	\centering
	\includegraphics{images/buffer_test_dynamic.pdf}
	\caption{RR sequence on a single file with different data sizes}
	\label{fig:buffer_test_dynamic}
\end{figure}

Interesting enough is the time step (between the datasizes of 16-32 Bytes) at the beginning, without any size incremention at all.

\subsubsection{Event based thread synchronization}
To enable smooth cooperation between the kernel module and the userspace programm, it is not only important to transfer data reliably and with good performance. There are also other aspects that are needed for good coordination between both sides.
In contrast to the request-response tests above, the trigger for the data exchange is normally not initiated by the userspace programm, but by the kernel module. In a next step, the userspace programm must be notified about the newly provided data. This can be compared to an input pin of a microcontroller: how does the firmware notice that a voltage change happened at the pin? There are two possibilities for this case: \textit{polling} or \textit{event-based}.

By means of \textit{polling}, the medium is to be polled for new information in cyclic intervals, depending on the selected method (file based or shared memory). This is possible in principle and would be well suited in theory for combination with the read and write cycles of the fieldbus protocol. The problem with this solution, however, is the synchronisation of the parallel running thread loops.

As shown in Figure \ref{fig:sync_loop_based}, both the kernel module and the userspace programm run a continuous loop for data processing. Since the kernel module only receives data from the application at a certain time, it is important to synchronize the individual loops.

Which means, it must be ensured that both sides are synchronised to the same clock. One possibility is to send a signal from the kernel module to indicate a new loop iteration.


\begin{figure}[!h]
	\centering
	\footnotesize
	\begin{BVerbatim}
   +---------+            +-----------+
   | Kernel  |            | Userspace |
   +---------+            +-----------+
        |                        |
        |---------------+        |
    ^   |               |        |
    |   |   *PLL con-   |        |
   100  | trolled loop* |        |------------+
   us   |               |        |            |   /-----------+
    |   |               |        |            |-->| read data |
    V   |<--------------+        |            |   +-----------/
        |                        |            |   /-------------+
        |                        |            |-->| handle data |
        |---------------+        |            |   +-------------/
    ^   |               |        |  response  |
    |   |   *PLL con-   |<---X---|<-----------+
   100  | trolled loop* |    ^   |
   us   |               |    |   |
    |   |               |    +---+---- wrong timing possible
    V   |<--------------+        |
        |                        |
        V                        V
	\end{BVerbatim}
	\caption{loop based without synchronization}
	\label{fig:sync_loop_based}
\end{figure}

However, if the userspace programm receives a signal from the kernel, it would also be possible to use this as an external event to trigger an interrupt service routine. This corresponds to an \textit{event-based} program logic. In this case, no loop would be needed on the part of Userspace. Communicating "downstream" (program $\rightarrow$ kernel) would be also event-based as read and writing actions trigger additional callback methods in the kernel module. Figure \ref{fig:sync_event_based} shows such a process.

\begin{figure}[!h]
	\centering
	\footnotesize
	\begin{BVerbatim}
   +---------+             +-----------+
   | Kernel  |             | Userspace |
   +---------+             +-----------+
        |                         |
        |                   sync  |
        |----------------+------->|----------+
    ^   |                |        |          |   /-----------+
    |   |    *PLL con-   |        |          |-->| read data |
    |   |  trolled loop* |        |          |   +-----------/
   100  |                |        |          |   /-------------+
   us   |                |        |          |-->| handle data |
    |   |                |        |          |   +-------------/
    |   |                |        | response |
    |   |                |<-------|<---------+
    V   |                |        |
        |<---------------+        |
        V                         V
	\end{BVerbatim}
	\caption{event based thread synchronization}
	\label{fig:sync_event_based}
\end{figure}

The question now arises, which method would be suitable as a trigger to inform userspace programm about new available data. The following possibilities were examined:

\begin{itemize}
	\item \verb|select(), poll(), epoll()|
	\item \textit{inotify API}
	\item \textit{Signals}
\end{itemize}

It quickly became apparent that methods which react to filesytem events are not applicable, since \textit{sysfs} and \textit{procfs} do not provide conventional files and changes made by kernel modules to the internal data objects can not be noticed. This leaves the option via Signals.

However, it must be noted that communicating via signals costs extra time. In an additional simulation with 10,000 iterations, it was investigated what a time delay using signals would mean. Figure \ref{fig:mmap_signaling_durations} shows the average time for a signal to reach the receiver, as well as several exceptions. In the worst case, a delay of up to 10 microseconds can be expected.

\begin{figure}[!h]
	\centering
	\includegraphics{images/event_app_w_signaling.pdf}
	\caption{Signal sending time measurements}
	\label{fig:mmap_signaling_durations}
\end{figure}

For this simulation a signal was send periodically from a kernel module to a userspace program using the function \verb|send_sig_info()|. To measure the elapsed time the difference between two timestamps was taken and saved.

\begin{lstlisting}[language=C, label=send_sig_info, caption=timestamping send\_sig\_info() , escapechar=\%]
	sync_timer_1 = ktime_get ();
	if (send_sig_info (info.si_signo, &info, task) < 0) {
		pr_err ("error sending signal\n");
		return -1;
	}
	sync_timer_2 = ktime_get ();
\end{lstlisting}
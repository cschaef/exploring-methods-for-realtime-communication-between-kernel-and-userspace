set key box width 0.75 height 0.75 opaque
#set ylabel "request-response time (us)"

set grid
set termopt
set term png size 1280, 720
set yrange [45:75]
set xrange [0:128]

# print png
set term png size 1280, 720
set output "images/big_vs_small_data_compare.png"
set multiplot layout 2,1 #margins 0.1, 0.9, 0.1, 0.9 spacing 0.025, 0.1
set label "sending / receiving data with raising size" at 90,19
set label "sending / receiving data with static size" at 90,96
plot "buffer_test_static_data_size.dat" using 1 title "2048 Bytes", "buffer_test_static_data_size.dat" using 3 title "16 Bytes"
set xlabel "test cycles"
set ylabel "request-response time (us)" offset 0,10,0
plot "buffer_test_raising_data_size.dat" using 1 title "2048 Bytes", "buffer_test_raising_data_size.dat" using 3 title "16 Bytes"
unset multiplot

# print epslatex
set term epslatex color
set output "images/big_vs_small_data_compare.tex"
set multiplot layout 2,1
set label "sending / receiving data with raising size" at 90,19
set label "sending / receiving data with static size" at 90,96
plot "buffer_test_static_data_size.dat" using 1 title "2048 Bytes", "buffer_test_static_data_size.dat" using 3 title "16 Bytes"
set xlabel "test cycles"
set ylabel "request-response time (us)" offset 0,10,0
plot "buffer_test_raising_data_size.dat" using 1 title "2048 Bytes", "buffer_test_raising_data_size.dat" using 3 title "16 Bytes"
unset multiplot

\documentclass{beamer}
\usepackage[english]{babel}
\usepackage[style=ieee]{biblatex}
\usepackage{csquotes}
\usepackage{multicol}
\usepackage{listings}
\usepackage{comment}
\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{multicol}
\usepackage{booktabs}

\begin{comment}
For English only:
Content of the Presentation:
* Initial Problem: Which theoretical/practical set of questions is the starting point of your work?
* Research Interest: Why did you choose this topic? What motivatios you for this work?
* State of Research: What research into your topic has already been done? How will your research contribute to it?
* Research Question: What are the concrete research question to which your work provided a scientific answer?
* Theoretical Position: Based on what theory did you pose your research question and why?
* Aims of your Thesis: What results should your research generate? Who should it be useful for?
* Methodology: Which working steps and which approaches led to your results?
* Outcomes of your Thesis: What outcomes did you generate? Who should they be useful for?
* Lessions Learned?
\end{comment}

\bibliography{bibliography.bib}

% ------------------------------------------------------------------
% Styling
% ------------------------------------------------------------------
\setbeamerfont{footnote}{size=\tiny}
\usetheme{Boadilla}
\usecolortheme{seahorse}

% ------------------------------------------------------------------
% Title
% ------------------------------------------------------------------
\title{Real-Time Communication between Kernel- and Userspace}
\subtitle{Bachelor Thesis}
\author{Christian Schaefer}

\begin{document}
% ------------------------------------------------------------------
% Title page
% ------------------------------------------------------------------
\begin{frame}
	\titlepage
\end{frame}

% ------------------------------------------------------------------
% Table of contents
% ------------------------------------------------------------------
\section*{Outline}
\begin{frame}
	\frametitle{Outline}
	\tableofcontents
\end{frame}

% ------------------------------------------------------------------
% Overview
% ------------------------------------------------------------------
\section{Problem Statement}
\subsection{Introduction}
\begin{frame}
	\frametitle{How it all started \dots}
	\framesubtitle{Problem Statement}
	\begin{itemize}
		\item bachelor project on behalf of \textit{Sigmatek}
		\item fieldbus communication protocol \textit{VARAN}\footnotemark for realtime industrial machine automation
		\item Evaluation board and Raspberry Pi header print were developed to test VARAN in existing networks
		\item custom Linux driver (\textit{vm111}) was created to handle the communication between the header and the evaluation board
	\end{itemize}
	\footnotetext{Versatile Automation Random Access Network}
\end{frame}

\subsection{}
\begin{frame}
	\frametitle{the smallest possible VARAN network}
	\framesubtitle{Problem Statement}
	\begin{figure}
		\begin{minipage}{.5\textwidth}
			\centering
			\includegraphics[width=.8\linewidth]{network-1.jpeg}
			\caption{VARAN Net w/ RPi and Evaluation Board}
		\end{minipage}%
		\begin{minipage}{.5\textwidth}
			\centering
			\includegraphics[width=.8\linewidth]{raspi-1.jpeg}
			\caption{RPi4 with VARAN Header (connected via SPI)}
		\end{minipage}
	\end{figure}
\end{frame}

\subsection{}
\begin{frame}[fragile=singleslide]
	\frametitle{What \emph{is} the problem now?}
	\framesubtitle{Problem Statement}
	\begin{itemize}
		\item How to communicate data from the driver to a userspace program?
		\item it must be guaranteed that communicating to the userspace does not bottleneck the real-time network
		\item Linux is not a real-time operating system
		\item internal process handling are not time deterministic, as GPOS\footnotemark are very complex
		\item PREEMPT\_RT patch to the rescue!
		\item critical kernel code sections are made preemptible
		\item provides real-time scheduling options (\verb|SCHED_DEADLINE|) for tasks
		\begin{itemize}
			\item dedicated task runtime period without any interference of other tasks
			\item can be used for periodic tasks and guarantee that they are not exceeding their scheduled time
		\end{itemize}
	\end{itemize}
	\footnotetext{General Purpose Operating Systems}
\end{frame}

\subsection{Research Questions}
\begin{frame}
	\frametitle{Deducing research questions}
	\framesubtitle{Problem Statement}
	\begin{itemize}
		\item how to transfer data from the Kernel to userspace in real-time?
		\item which methods are available?
		\item what are their advantages and disadvantages?
		\item which method is suited most for the \textit{Sigmateks} kernel driver?
	\end{itemize}
\end{frame}

\section{Experiments}
\subsection{Available Methods and Requirements}
\begin{frame}[fragile]
	\frametitle{Let's get down to business}
	\framesubtitle{Experiments}
	\begin{itemize}
		\item four potential methods analyzed:
		\begin{itemize}
			\item netlink (socket based IPC)
			\item virtual filesystems: procfs and sysfs (read \& write of virtual files)
			\item shared memory (zero copy mechanism)
		\end{itemize}
		\item kernel module and respective userspace tool implemented
		\item test approach: timing of a request-response sequence
		\item including different scheduling policies (\verb|SCHED_DEADLINE|, \verb|SCHED_RR|, \verb|SCHED_FIFO|, \verb|SCHED_OTHER|)
		\item real-time requirements:
		\begin{itemize}
			\item \textless 100 us
			\item 2048 Bytes
		\end{itemize}
	\end{itemize}
\end{frame}

\subsection{Test Procedure}
\begin{frame}[fragile]
	\frametitle{Request-Response Sequence}
	\framesubtitle{Experiments}
	\begin{figure}[!h]
		\centering
		\footnotesize
		\begin{BVerbatim}
        +---------------+                       +---------------+
        | Userspace-App |                       | Kernel-Module |
        +-------+-------+                       +-------+-------+
start timer t_1 |-----------+                           |
                |           |                           |
                |<----------+                           |
                | send communication request (2048 B) d |
                |-------------------------------------->|
start timer t_2 |-----------+                           |
                |           |                           |
                |<----------+                           |
                |               send response (2048 B)  |
                |<--------------------------------------|
stop timer t_1, |-----------+                           |
           t_2  |           |                           |
                |<----------+                           |
		\end{BVerbatim}
		\caption{Measurement sequence diagram }
		\label{fig:measurement_sequence_diagram}
	\end{figure}
\end{frame}

\section{Outcomes}
\subsection{Results}
\begin{frame}
	\frametitle{Experiment results\footnotemark}
	\framesubtitle{Outcomes}
	\begin{table}[h!]
		\centering
		\begin{tabular}{@{}lllllll@{}}
					& \multicolumn{2}{l}{SCHED\_DEADLINE} & \multicolumn{2}{l}{SCHED\_RR} & \multicolumn{2}{l}{SCHED\_FIFO}                                      \\\cmidrule(lr){2-3}\cmidrule(lr){4-5}\cmidrule(lr){6-7}
					& max                                 & average                       & max                             & average   & max        & average   \\ \midrule
			netlink & 302.72                          & 90.91                     & 154.77                      & 89.80 & 151.70 & 89.94 \\
			sysfs   & 72.86                           & 36.11                     & 74.10                       & 35.69 & 76.16  & 35.64 \\
			procfs  & 17.75                           & 08.39                     & 28.01                       & 08.14 & 22.05  & 08.20 \\
			mmap    & 22.94                           & 11.04                     & 26.37                       & 10.90 & 31.35  & 10.97 \\ \bottomrule
		\end{tabular}
		\caption{Request-Response sequence times (us) using different scheduling algorithms}
	\end{table}
	\footnotetext[3]{of 10.000 test runs with each method and each scheduling policy}
\end{frame}

\begin{frame}
	\frametitle{Experiment results visualized}
	\framesubtitle{Outcomes}
	\begin{figure}
			\centering
			\includegraphics[width=.8\linewidth]{results-chart.png}
			\caption{Duration maximums of 10.000 tests}
	\end{figure}
\end{frame}


\subsection{Conclusion}
\begin{frame}
	\frametitle{Conclusion}
	\framesubtitle{Outcomes}
	\begin{itemize}
		\item \textbf{procfs}: best performance, read-write lock necessary, appropriate data format needed
		\item \textbf{shared memory}: fast, no data copying, memory access management needed, prone to errors
		\item \textbf{sysfs}: slowest (possible) method, similar to \textbf{procfs}, elegant solution to represent data objects (each property of a struct has its own file)
		\item \textbf{netlink}: too slow, not applicable for this usecase
	\end{itemize}
\end{frame}

\subsection{Further Work}
\begin{frame}
	\frametitle{It's not over!}
	\framesubtitle{Outcomes}
	\begin{itemize}
		\item more detailed analysis necessary (which data really needs to be transfered to userspace)
		\item evaluate real-time requirements again
		\item synchronization between kernel- and userthreads
		\item data access management / locking mechanism needed
	\end{itemize}
\end{frame}

\subsection{Lessions learned}
\begin{frame}
	\frametitle{Lessions learned}
	\framesubtitle{Outcomes}
	\begin{itemize}
		\item "I know that I know nothing" (c) Socrates
		\item early start prevents stress at the end
		\item if part-time: work on a project with your company
	\end{itemize}
\end{frame}

\end{document}
set grid
set termopt

set logscale y 2
set xlabel "signaling time (us)"
set ylabel "relative frequency (0-1)"
set bars fullwidth
set style data histograms
set xtics (0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22)
set xrange [0:22]

# define time range (us)
bin_width = 0.1;
bin_number(x) = floor(x/bin_width)
bin(x) = bin_width * ( bin_number(x) + 0.5 )

set boxwidth bin_width
set style fill transparent solid 0.4

# png stuff
set term png truecolor size 1280, 720
set output "images/event_app_w_signaling.png"
set terminal png noenhanced

plot "mmap_event_log_deadline.dat" using (bin($4)):(1) smooth fnormal with boxes title "10.000x sending via send_sig_info()"


set term pdfcairo color font "times, 12"
set terminal pdfcairo noenhanced
set output "images/event_app_w_signaling.pdf"
plot "mmap_event_log_deadline.dat" using (bin($4)):(1) smooth fnormal with boxes title "10.000x sending via send_sig_info()"
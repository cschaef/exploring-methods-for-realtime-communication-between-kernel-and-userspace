# plot buffer_test_static_data_size.dat histogram
set grid
set termopt 
set ylabel "relative frequency"
set xlabel "request-response (us)"
set bars fullwidth
set style data histograms
set logscale y 2

# get stats of all files
stats "buffer_test_static_data_size.dat" name "static_statics" nooutput

# define time range (us)
bin_width = 0.5;
bin_number(x) = floor(x/bin_width)
bin(x) = bin_width * ( bin_number(x) + 0.5 )

set boxwidth bin_width
set style fill transparent solid 0.5

# png stuff
set term png truecolor size 1280, 720
set output "images/buffer_test_static.png"
plot "buffer_test_static_data_size.dat" using (bin($1)):(1) smooth fnormal with boxes title "2048 Bytes", \
     "buffer_test_static_data_size.dat" using (bin($2)):(1) smooth fnormal with boxes title "16 Bytes"


set term pdfcairo color font "times, 12"
set output "images/buffer_test_static.pdf"
plot "buffer_test_static_data_size.dat" using (bin($1)):(1) smooth fnormal with boxes title "2048 Bytes", \
     "buffer_test_static_data_size.dat" using (bin($2)):(1) smooth fnormal with boxes title "16 Bytes"

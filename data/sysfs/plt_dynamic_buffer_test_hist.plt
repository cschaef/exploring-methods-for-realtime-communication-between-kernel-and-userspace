# plot buffer_test_static_data_size.dat histogram
# grid stuff
set grid
set x2range [16:4096]
set x2tics scale 0.0 format ""  # show no x2 tics or labels on the plot
set logscale x2 2
set grid x2 nox

set xtics ("16" 0, "32" 128, "64" 256, "128" 384, "256" 512 , "512" 640, "1024" 768, "2048" 896)


set termopt 
set xlabel "data size (Bytes)"
set ylabel "time (us)"

set style data histograms
set style fill transparent solid 0.5

# get stats of all files
stats "buffer_test_dynamic_data_size_single_file.dat" name "static_statics" nooutput

# png stuff
set term png truecolor size 1280, 720
set output "images/buffer_test_dynamic.png"
plot "buffer_test_dynamic_data_size_single_file.dat" using 1 title "request-response duration times" lc rgb "slategrey"

set term pdfcairo color font "times, 12"
set output "images/buffer_test_dynamic.pdf"
plot "buffer_test_dynamic_data_size_single_file.dat" using 1 title "request-response duration times" lc rgb "slategrey"

set grid
set termopt

set logscale y 2
set xlabel "signaling time (us)"
set ylabel "relative frequency"
set bars fullwidth
set style data histograms

# define time range (us)
bin_width = 0.1;
bin_number(x) = floor(x/bin_width)
bin(x) = bin_width * ( bin_number(x) + 0.5 )

set boxwidth bin_width
set style fill transparent solid 0.4

# png stuff
set term png truecolor size 1280, 720
set output "images/event_app_w_signaling.png"
set terminal png noenhanced

plot "event_app_w_signaling.dat" using (bin($4)):(1) smooth fnormal with boxes title "10.000x sending via send_sig_info()"

set term pdfcairo noenhanced  color font "times, 12"
set output "images/event_app_w_signaling.pdf"
plot "event_app_w_signaling.dat" using (bin($4)):(1) smooth fnormal with boxes title "10.000x sending via send\_sig\_info()"
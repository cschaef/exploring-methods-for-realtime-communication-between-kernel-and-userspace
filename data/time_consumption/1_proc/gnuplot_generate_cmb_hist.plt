set grid
set termopt
set logscale y 2
set logscale x 10
set xrange [:75]
set xtics (0, 10, 25, 50, 75, 100, 150, 200)
set ytics (0.0001, 0.001, 0.01, 0.1, 0.25, 0.5, 0.75, 1)

set xlabel "request-response time (us)"
set ylabel "relative frequency"
set bars fullwidth
set style data histograms

# get stats of all files
stats "request-response-1_proc-OTHER.dat" name "OTHER" nooutput
stats "request-response-1_proc-FIFO.dat" name "FIFO" nooutput
stats "request-response-1_proc-RR.dat" name "RR" nooutput
stats "request-response-1_proc-DEADLINE.dat" name "DEADLINE" nooutput


# define time range (us)
bin_width = 0.2;
bin_number(x) = floor(x/bin_width)
bin(x) = bin_width * ( bin_number(x) + 0.5 )

set boxwidth bin_width
set style fill transparent solid 0.4

# png stuff
set term png truecolor size 1280, 720
set output "images/combined_request-response-1_proc.png"
plot "request-response-1_proc-OTHER.dat" using (bin($1)):(1) smooth fnormal with boxes title "OTHER",  "request-response-1_proc-FIFO.dat" using (bin($1)):(1) smooth fnormal with boxes title "FIFO", "request-response-1_proc-RR.dat" using (bin($1)):(1) smooth fnormal with boxes title "RR", "request-response-1_proc-DEADLINE.dat" using (bin($1)):(1) smooth fnormal with boxes title "DEADLINE"

set term pdfcairo color font "times, 12"
set output "images/combined_request-response-1_proc.pdf"
plot "request-response-1_proc-OTHER.dat" using (bin($1)):(1) smooth fnormal with boxes title "OTHER", "request-response-1_proc-FIFO.dat" using (bin($1)):(1) smooth fnormal with boxes title "FIFO", "request-response-1_proc-RR.dat" using (bin($1)):(1) smooth fnormal with boxes title "RR", "request-response-1_proc-DEADLINE.dat" using (bin($1)):(1) smooth fnormal with boxes title "DEADLINE"

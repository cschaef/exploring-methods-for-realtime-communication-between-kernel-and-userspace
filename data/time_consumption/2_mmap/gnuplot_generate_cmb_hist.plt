set grid
set termopt
set logscale y 2
set logscale x 10
set xrange [:70]
set xtics (10, 25, 50, 75, 100, 150, 200)
set ytics (0.00001, 0.0001, 0.001, 0.01, 0.1, 0.25, 0.5, 0.75, 1)

set xlabel "request-response time (us)"
set ylabel "relative frequency"
set bars fullwidth
set style data histograms

# get stats of all files
stats "request-response-2_mmap-OTHER.dat" name "OTHER" nooutput
stats "request-response-2_mmap-FIFO.dat" name "FIFO" nooutput
stats "request-response-2_mmap-RR.dat" name "RR" nooutput
stats "request-response-2_mmap-DEADLINE.dat" name "DEADLINE" nooutput


# define time range (us)
bin_width = 0.25;
bin_number(x) = floor(x/bin_width)
bin(x) = bin_width * ( bin_number(x) + 0.5 )

set boxwidth bin_width
set style fill transparent solid 0.5

# png stuff
set term png truecolor size 1280, 720
set output "images/combined_request-response-2_mmap.png"
plot "request-response-2_mmap-OTHER.dat" using (bin($1)):(1) smooth fnormal with boxes title "OTHER", "request-response-2_mmap-FIFO.dat" using (bin($1)):(1) smooth fnormal with boxes title "FIFO", "request-response-2_mmap-RR.dat" using (bin($1)):(1) smooth fnormal with boxes title "RR", "request-response-2_mmap-DEADLINE.dat" using (bin($1)):(1) smooth fnormal with boxes title "DEADLINE"

set term pdfcairo color font "times, 12"
set output "images/combined_request-response-2_mmap.pdf"
plot "request-response-2_mmap-OTHER.dat" using (bin($1)):(1) smooth fnormal with boxes title "OTHER", "request-response-2_mmap-FIFO.dat" using (bin($1)):(1) smooth fnormal with boxes title "FIFO", "request-response-2_mmap-RR.dat" using (bin($1)):(1) smooth fnormal with boxes title "RR", "request-response-2_mmap-DEADLINE.dat" using (bin($1)):(1) smooth fnormal with boxes title "DEADLINE"

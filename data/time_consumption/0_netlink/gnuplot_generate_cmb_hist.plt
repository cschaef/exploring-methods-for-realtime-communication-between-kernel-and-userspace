set grid
set termopt
set logscale y 2
set logscale x 10
set xrange [70:350]
set yrange [:1]
set xtics (15, 25, 50, 75, 100, 250, 500)
set ytics (0.0001, 0.001, 0.01, 0.1, 0.25, 0.5, 0.75, 1)

set xlabel "request-response time (us)"
set ylabel "relative frequency"
set bars fullwidth
set style data histograms

# get stats of all files
stats "request-response-0_netlink-OTHER.dat" name "OTHER" nooutput
stats "request-response-0_netlink-FIFO.dat" name "FIFO" nooutput
stats "request-response-0_netlink-RR.dat" name "RR" nooutput
stats "request-response-0_netlink-DEADLINE.dat" name "DEADLINE" nooutput

# define time range (us)
bin_width = 1.5;
bin_number(x) = floor(x/bin_width)
bin(x) = bin_width * ( bin_number(x) + 0.5 )

set boxwidth bin_width
set style fill transparent solid 0.4

# png stuff
set term png truecolor size 1280, 720
set output "images/combined_request-response-0_netlink.png"
plot "request-response-0_netlink-OTHER.dat" using (bin($1)):(1) smooth fnormal with boxes title "OTHER", "request-response-0_netlink-FIFO.dat" using (bin($1)):(1) smooth fnormal with boxes title "FIFO", "request-response-0_netlink-RR.dat" using (bin($1)):(1) smooth fnormal with boxes title "RR", "request-response-0_netlink-DEADLINE.dat" using (bin($1)):(1) smooth fnormal with boxes title "DEADLINE"

set term pdfcairo color font "times, 12"
set output "images/combined_request-response-0_netlink.pdf"
plot "request-response-0_netlink-OTHER.dat" using (bin($1)):(1) smooth fnormal with boxes title "OTHER", "request-response-0_netlink-FIFO.dat" using (bin($1)):(1) smooth fnormal with boxes title "FIFO", "request-response-0_netlink-RR.dat" using (bin($1)):(1) smooth fnormal with boxes title "RR", "request-response-0_netlink-DEADLINE.dat" using (bin($1)):(1) smooth fnormal with boxes title "DEADLINE"

/*
 * @references:
 *	https://elixir.bootlin.com/linux/v5.5.19/source/include/linux/mm_types.h#L292
 * 	https://linux-kernel-labs.github.io/refs/heads/master/labs/memory_mapping.html
 */

#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h> /* min */
#include <linux/ktime.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/signal.h>
#include <linux/signal_types.h>
#include <linux/slab.h>
#include <linux/timekeeping.h>
#include <linux/timer.h>
#include <linux/uaccess.h> /* copy_from_user, copy_to_user */
#include <linux/version.h>
#include <uapi/linux/sched/types.h>

#include "../include/sig_common.h"
#include "mmap_common.h"

#define pr_fmt(fmt) "%s:%s: " fmt, KBUILD_MODNAME, __func__

static DEFINE_MUTEX (g_mutex);
static struct timer_list timer;
char *shared_memory;
int __do_periodic_cycle		      = 1;
static char *procfs_subscriber_buffer = NULL;
static char *procfs_sig_time_buffer   = NULL;
static struct proc_dir_entry *ent, *subscribe_ent, *sig_time_ent;
int subscriber_pid = 0;
size_t data_len_subscriber, data_len_sig_time;

static void
memory_write (void *address, char *buffer, size_t buffer_len)
{
	pr_devel ("Writing <%s> (%zu) to addr: %p\n", buffer, buffer_len, address);
	memcpy (address, buffer, buffer_len);
}

/**
 * MMAP handler - gets called when userland program creates a new memory region
 * @param fil pointer to a struct file created when the device is opened from user space.
 * @param vma used to indicate the virtual address space where the memory should be mapped by the
 * device
 * @return 0 if successful
 */
static int
file_mmap (struct file *filp, struct vm_area_struct *vma)
{
	struct page *page  = NULL;
	unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);

	pr_devel ("mmap\n");

	/* check if desired size is too big */
	if (size > PAGE_SIZE) {
		pr_err ("desired memory length is too big, max size is %lu\n", PAGE_SIZE);
		return -EINVAL;
	}

	/* support only shared mappings - deny copy on write */
	if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED)) {
		pr_err ("writeable mappings must be shared, rejecting\n");
		return (-EINVAL);
	}

	/* we do not want to have this area swapped out, lock it */
	vma->vm_flags |= VM_LOCKED;

	/*
	shared_memory	      = vma;
	shared_memory->vm_ops = &vm_ops;
	shared_memory->vm_flags |= VM_DONTEXPAND | VM_DONTDUMP;
	shared_memory->vm_private_data = filp->private_data;
	*/

	/* get page of the shared_memory area */
	page = virt_to_page ((unsigned long)shared_memory + (vma->vm_pgoff << PAGE_SHIFT));

	/* map a contiguous physical address space (indicated by page) into the virtual space (vma)
	 */
	if (remap_pfn_range (vma, vma->vm_start, page_to_pfn (page), size, vma->vm_page_prot)
	    != 0) {
		pr_err ("failure while mapping shared memory to userspace\n");
		return -ENOMEM;
	}

	pr_devel ("successfully mmap'd shared memory \n");
	return 0;
}

/**
 * file open handler
 * @param inode
 * @param filep
 * @return 0 if successful
 */
static int
file_open (struct inode *inode, struct file *filp)
{
	pr_devel ("open <%s> \n", filp->f_path.dentry->d_iname);

	if (!mutex_trylock (&g_mutex)) {
		pr_alert ("device locked - cannot open\n");
		return -EBUSY;
	}
	return 0;
}

/**
 * @brief allocates shared memory with PAGE_SIZE on the heap
 * @return 0 if successfull
 */
int
prepare_shared_memory (int npages)
{
	int i;

	pr_devel ("allocating %lu bytes for shared memory\n", PAGE_SIZE);
	shared_memory = kmalloc (PAGE_SIZE * npages, GFP_ATOMIC);
	if (shared_memory == NULL) {
		pr_err ("failure while allocating shared_memory\n");
		return -ENOMEM;
	}

	/* set reserved bit to avoid pages being "swapped" out ? */
	for (i = 0; i < npages * PAGE_SIZE; i += PAGE_SIZE) {
		SetPageReserved (virt_to_page (((unsigned long)shared_memory) + i));
	}

	/* fill memory with dummy data */
	memset (shared_memory, 0, PAGE_SIZE);

	return 0;
}

/**
 * @brief frees the allocated memory and resets the page reserved bit
 * @return 0
 */
int
release_shared_memory (int npages)
{
	int i;

	pr_devel ("freeing shared memory\n");
	for (i = 0; i < npages * PAGE_SIZE; i += PAGE_SIZE) {
		ClearPageReserved (virt_to_page (((unsigned long)shared_memory) + i));
	}
	kfree (shared_memory);
	return 0;
}

/**
 * @brief read handler
 * @param filp
 * @param buf
 * @param len
 * @param off
 * @return
 */
static ssize_t
file_read (struct file *filp, char __user *buf, size_t len, loff_t *off)
{
	pr_devel ("file read handler called\n");
	/* check for read overflow */
	if (len > PAGE_SIZE) {
		pr_err ("overflow read failure\n");
		return -EFAULT;
	}

	if (copy_to_user (buf, shared_memory, len) == 0) {
		pr_devel ("outgoing: %s\n", shared_memory);
		return len;
	}
	return -EFAULT;
}

/**
 * @brief write handler, copies (?) buffer content to shared memory
 * @param filp
 * @param buf
 * @param len
 * @param off
 * @return
 */
static ssize_t
file_write (struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
	pr_devel ("file write handler called\n");
	if (len >= WRITE_AREA_LEN) {
		return -EMSGSIZE;
	}

	// if (copy_from_user (filp->private_data->data, buf, len))
	if (copy_from_user (shared_memory, buf, len)) {
		pr_err ("failure while writing to lkm\n");
		return -EFAULT;
	}

	pr_devel ("incoming: %s\n", shared_memory);
	return len;
}

/**
 * @brief release handler
 * @param inode
 * @param filp
 */
static int
file_release (struct inode *inode, struct file *filp)
{
	pr_devel ("release handler called\n");
	mutex_unlock (&g_mutex);

	/*
		struct mmap_info *info;
		info = filp->private_data;
		free_page ((unsigned long)info->data);
		kfree (info);
		filp->private_data = NULL;
	*/
	return 0;
}

static ssize_t
subscribe_write (struct file *file, const char __user *buff, size_t count, loff_t *offp)
{
	int ret;
	pr_info ("called. Saving <%s> (%d) internally\n", buff, count);
	if (count > MAX_DATA_OBJECT_LEN) {
		pr_err ("message too long\n");
		return -EFAULT;
	}
	memset (procfs_subscriber_buffer, 0, 32);
	memcpy (procfs_subscriber_buffer, buff, count);
	*offp		    = (int)count;
	data_len_subscriber = count - 1;

	/* set subscriber pid */
	pr_info ("procfs_subscriber_buffer: <%s>\n", procfs_subscriber_buffer);

	ret = kstrtoint (procfs_subscriber_buffer, 10, &subscriber_pid);
	if (ret != 0) {
		pr_err ("cannot convert str to process id: <%s> err: %d\n",
			procfs_subscriber_buffer, ret);
		return -EFAULT;
	}
	pr_info ("new subscriber: %d\n", subscriber_pid);

	return count;
}

static ssize_t
sig_time_write (struct file *file, const char __user *buff, size_t count, loff_t *offp)
{
	return 0;
}

static ssize_t
subscribe_read (struct file *file, char __user *buff, size_t count, loff_t *offp)
{
	if ((int)(*offp) > data_len_subscriber) {
		return 0;
	}

	if (count == 0) {
		pr_warn ("nothing to read\n");
		return count;
	}
	count = data_len_subscriber + 1; /* add \0 character */

	/* copy content to userspace */
	memcpy (buff, procfs_subscriber_buffer, 32);
	*offp = count;
	return count;
}

static ssize_t
sig_time_read (struct file *file, char __user *buff, size_t count, loff_t *offp)
{
	if ((int)(*offp) > data_len_sig_time) {
		return 0;
	}

	if (count == 0) {
		pr_warn ("nothing to read\n");
		return count;
	}
	count = data_len_sig_time + 1; /* add \0 character */

	/* copy content to userspace */
	memcpy (buff, procfs_sig_time_buffer, 32);
	*offp = count;
	return count;
}

/* with kernel version 5.6.0 proc_create() uses a proc_ops struct instead of file_operations */
#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 6, 0)
static const struct file_operations fops = {
	.mmap	 = file_mmap,
	.open	 = file_open,
	.release = file_release,
	.read	 = file_read,
	.write	 = file_write,
};
static struct file_operations fops_subscribe = { .read = subscribe_read, .write = subscribe_write };
static struct file_operations fops_sig_time  = { .read = sig_time_read, .write = sig_time_write };

#else
static const struct proc_ops fops = {
	.proc_mmap    = file_mmap,
	.proc_open    = file_open,
	.proc_release = file_release,
	.proc_read    = file_read,
	.proc_write   = file_write,
};
static const struct proc_ops fops_subscribe
	= { .proc_read = subscribe_read, .proc_write = subscribe_write };
static const struct proc_ops fops_sig_time
	= { .proc_read = sig_time_read, .proc_write = sig_time_write };
#endif

/**
 * Creates a /proc entry
 * @return 0 if successfull
 */
int
create_proc_entry (void)
{
	/* creates /proc directory entry */
	ent = proc_create (MODULE_NAME, PERMISSIONS, NULL, &fops);
	if (ent == NULL) {
		pr_err ("could not create /proc file\n");
		return 1;
	}
	return 0;
}

static int
sync_subscriber_process (void)
{
	ktime_t sync_timer;
	struct task_struct *task;
	struct pid *pid_struct = find_get_pid (subscriber_pid);

	if (pid_struct == NULL) {
		pr_debug ("no subscriber pid registered\n");
		return 0;
	}
	task = pid_task (pid_struct, PIDTYPE_PID);

#if LINUX_VERSION_CODE <= KERNEL_VERSION(4, 19, 71)
	struct siginfo info;
	memset (&info, '\0', sizeof (struct siginfo));
#else
	struct kernel_siginfo info;
	memset (&info, '\0', sizeof (struct kernel_siginfo));
#endif

	info.si_signo = SIG_SYNC;
	info.si_code  = SI_QUEUE;

	sync_timer = ktime_get ();
	if (send_sig_info (info.si_signo, &info, task) < 0) {
		pr_err ("error sending signal\n");
		return -1;
	}
	sync_timer = ktime_get () - sync_timer;
	sprintf (procfs_sig_time_buffer, "%lld", sync_timer);

	// pr_info ("sending signal %d, sig duration: %lld\n", SIG_SYNC, sync_obj->timestamp);
	return 0;
}
/**
 * periodic routine to write a timestamp to data_0 attribute of the first do2 object
 */
static void
start_event_worker (struct timer_list *unused)
{
	sync_subscriber_process ();
	mod_timer (&timer, jiffies + usecs_to_jiffies (CYCLE_INTERVAL_US));
}

/**
 * create a /proc entry for subscriptions and the signal timekeeping
 * @return 0 if successfull
 */
int
create_helper_proc_entry (void)
{
	/* set initial data length */
	data_len_subscriber = 0;
	data_len_sig_time   = 0;

	procfs_subscriber_buffer = kmalloc (32, GFP_ATOMIC);
	procfs_sig_time_buffer	 = kmalloc (32, GFP_ATOMIC);

	if (procfs_subscriber_buffer == NULL || procfs_sig_time_buffer == NULL) {
		pr_err ("could not allocate memory for data buffer\n");
		return -ENOMEM;
	}

	/* creates /proc directory entry */
	subscribe_ent = proc_create_data (SUBSCRIBE_FILE, PERMISSIONS, NULL, &fops_subscribe,
					  procfs_subscriber_buffer);
	sig_time_ent  = proc_create_data (SIG_FILE, PERMISSIONS, NULL, &fops_sig_time,
					  procfs_sig_time_buffer);

	if (subscribe_ent == NULL) {
		return -1;
	}
	return 0;
}

/**
 * @brief Entry routine for the kernel module
 * @return
 */
static int __init
run (void)
{
	pr_info ("startup\n");
	mutex_init (&g_mutex);
	if (create_proc_entry ()) {
		pr_err ("cannot create /proc entry\n");
		return -1;
	}

	if (create_helper_proc_entry () != 0) {
		pr_err ("cannot create /proc/mmap_subscribe entry\n");
		return -1;
	}

	if (prepare_shared_memory (1) != 0) {
		pr_err ("failure whilst preparing shared memory\n");
		return -1;
	}

	if (set_scheduling_policy () < 0) {
		pr_err ("cannot set scheduling policy\n");
		// return -1;
	}

	if (__do_periodic_cycle == 1) {
		timer_setup (&timer, start_event_worker, 0);
		mod_timer (&timer, jiffies + usecs_to_jiffies (CYCLE_INTERVAL_US));
	}

	pr_devel ("Setup done, starting periodic task\n");
	return 0;
}

/**
 * Exit routine, called when module is removed
 */
static void
cleanup (void)
{
	pr_info ("release\n");
	del_timer (&timer);
	release_shared_memory (1);
	proc_remove (ent);
	proc_remove (subscribe_ent);
	proc_remove (sig_time_ent);
	mutex_destroy (&g_mutex);
}

module_init (run);
module_exit (cleanup);
MODULE_LICENSE ("GPL");

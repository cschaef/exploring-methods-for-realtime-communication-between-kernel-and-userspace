/*
 * @references
 * 	https://github.com/cirosantilli/linux-kernel-module-cheat
 * 	https://www.man7.org/linux/man-pages/man2/mmap.2.html
 */

#include <errno.h>
#include <fcntl.h>
#include <linux/types.h>
#include <sched.h>
#include <stdint.h> /* uintmax_t */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h> /* sysconf */

#define PROGRAM_NAME "mmap_application"

#include "../include/app_common.h"
#include "../include/sig_common.h"
#include "mmap_common.h"

int fd	       = 0;
long page_size = 0;
int write_cnt  = 0;
void *mem      = NULL;

void
page_dump (void *address)
{
	const uint8_t *ptr = address;
	for (size_t i = 0; i < page_size; i++) {
		printf ("%c", *ptr);
		ptr++;
	}
}

void
memory_dump (void *address, size_t shared_memory_len)
{
	for (size_t i = 0; i < (shared_memory_len / page_size); i++) {
		page_dump (address + (i * page_size));
	}
}

int
write_to_mmap (void *mem, const char *message, size_t message_len)
{
	if (message_len > WRITE_AREA_LEN) {
		printf ("Message to big, cannot write into send buffer len: %zu\n", message_len);
		return 1;
	}

	if (write (fd, message, message_len) < 0) {
		perror ("error writing");
		exit (EXIT_FAILURE);
	}

	write_cnt++;
	return 0;
}
/**
 * @brief reacts on the read message and sends the appropiate message back
 * @param incoming read message
 * @param outgoing new msg to be send to the lkm
 */
void
message_handler (struct mmap_message *incoming)
{
	printf ("%s start\n", __func__);
}

/**
 * @brief reads `len` bytes from area `mem` into struct mmap_message
 * @param mem area to read from
 * @param len size of the to be read area
 * @param msg object buffer to save the read message
 * @return NULL ... if no response is needed
 *         object ... if response is needed
 */
int
read_from_mmap (void *mem, char *buffer, size_t len)
{
	if (read (fd, buffer, len) < 0) {
		perror ("error reading");
		return -1;
	}
	return 0;
}

/**
 * @brief opens (or creates if not existing) a file at `MMAP_FILE` and truncates it to a length of
 * `MEMORY_AREA_LEN`
 */
void
prepare_mmap_file (void)
{
	/* get a file descriptor, create a file if it does not exists, truncate to 0 if it exists,
	 * set R&W rights */
	// fd = open (MMAP_FILE, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	fd = open (MMAP_FILE, O_RDWR, S_IRUSR | S_IWUSR);

	if (fd <= 0) {
		printf ("Error opening file %s (%d)\n", MMAP_FILE, errno);
		exit (EXIT_FAILURE);
	}

	/* truncate the file to our needed size */
	if (ftruncate (fd, MEMORY_AREA_LEN) != 0) {
		printf ("Error truncating the file (%d)\n", errno);
		exit (EXIT_FAILURE);
	}

	/* write an empty byte to the end of the file, so the file has actually the correct size
	 * https://stackoverflow.com/questions/27697228/mmap-and-struct-in-c
	 */
	lseek (fd, 0, SEEK_END);
	write (fd, "", 1);
	lseek (fd, 0, SEEK_SET);
}

/**
 * @brief maps the fd file into the memory
 * @param shared_memory area which the fd was mapped into
 */
void
prepare_shared_mem (void *shared_memory)
{
	page_size     = sysconf (_SC_PAGE_SIZE);
	shared_memory = mmap (NULL, MEMORY_AREA_LEN, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if (shared_memory == MAP_FAILED) {
		printf ("Error creating shared map (%d)\n", errno);
		exit (EXIT_FAILURE);
	}
}

int
my_send (struct payload_data_object *obj)
{
	return write_to_mmap (mem + WRITE_AREA_OFF, obj->data_ptr, obj->data_len);
}

int
my_recv (struct payload_data_object *obj)
{
	return read_from_mmap (mem, obj->data_ptr, MAX_DATA_OBJECT_LEN);
}

// make app && sudo ./mmap_application.out -c 100 -w 800 -d 1000 -p 1000 -s DEADLINE

/**
 * @brief main entry routine, runs init functions and starts the worker thread
 * @return 0 on success
 */
int
main (int argc, char **argv)
{
	parse_args (argc, argv);

	struct timer_info *timers = NULL;
	struct payload_data_object data
		= { .data_len = g_len, .data_ptr = (char *)malloc (g_len), .status = 0 };

	/* init functions */
	generate_random_data (&data);
	prepare_mmap_file ();
	prepare_shared_mem (mem);
	if (set_schedule (g_schedule_policy, 90, g_process_time, g_deadline_time, g_period_time)
	    != 0) {
		printf ("error setting schedule policy (%d)\n", errno);
		exit (EXIT_FAILURE);
	}

	timers = (struct timer_info *)malloc (sizeof (struct timer_info) * g_test_cycles);

	/* start worker */
	worker_thread (&data, timers, my_send, my_recv);

	/* print results and clean up */
	print_timers (timers, g_test_cycles);
	free (timers);
	munmap (mem, MEMORY_AREA_LEN);
	close (fd);
	return 0;
}

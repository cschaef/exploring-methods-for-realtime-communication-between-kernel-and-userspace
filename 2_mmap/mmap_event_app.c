/**
 * @file mmap_event_app.c
 * @brief test app which waits for a signal to start a RR cycle
 *
 */

#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <unistd.h>

#define PROGRAM_NAME "mmap_event_app"

#include "../include/app_common.h"
#include "mmap_common.h"

#undef MAX_DATA_OBJECT_LEN
#define MAX_DATA_OBJECT_LEN 2047

struct timespec start_app, start_kernel, end;
struct sigaction sig;
char sig_file_path[32];
int fd		= 0;
long page_size	= 0;
void *mem	= NULL;
int running	= 1;
int sig_counter = 0;
char send_buffer[MAX_DATA_OBJECT_LEN];
char recv_buffer[MAX_DATA_OBJECT_LEN];
#define MAX_LOOP 10000
char timer_array[MAX_LOOP][128];

int
write_to_mmap (const char *message, size_t message_len)
{
	if (message_len > WRITE_AREA_LEN) {
		printf ("Message to big, cannot write into send buffer len: %zu\n", message_len);
		return 1;
	}

	if (write (fd, message, message_len) < 0) {
		perror ("error writing");
		exit (EXIT_FAILURE);
	}
	return 0;
}

int
read_from_mmap (char *buffer, size_t len)
{
	if (read (fd, buffer, len) < 0) {
		perror ("error reading");
		return -1;
	}
	return 0;
}

float
get_sig_info_time_ns ()
{
	char signal_time_buffer[32];
	int sig_info_fd = open (sig_file_path, O_RDONLY);
	lseek (sig_info_fd, 0, SEEK_SET);
	read (sig_info_fd, signal_time_buffer, 4);
	close (sig_info_fd);
	return strtol (signal_time_buffer, NULL, 10);
}

int
prepare_mmap_file (void)
{
	/* get a file descriptor, create a file if it does not exists, truncate to 0 if it exists,
	 * set R&W rights */
	fd = open (MMAP_FILE, O_RDWR, S_IRUSR | S_IWUSR);

	if (fd <= 0) {
		printf ("Error opening file %s (%d)\n", MMAP_FILE, errno);
		return -1;
	}

	/* truncate the file to our needed size */
	if (ftruncate (fd, MEMORY_AREA_LEN) != 0) {
		printf ("Error truncating the file (%d)\n", errno);
		return -1;
	}

	/* write an empty byte to the end of the file, so the file has actually the correct size
	 * https://stackoverflow.com/questions/27697228/mmap-and-struct-in-c
	 */
	lseek (fd, 0, SEEK_END);
	write (fd, "", 1);
	lseek (fd, 0, SEEK_SET);
	return 0;
}

int
do_mmap ()
{
	page_size = sysconf (_SC_PAGE_SIZE);
	mem	  = mmap (NULL, MEMORY_AREA_LEN, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if (mem == MAP_FAILED) {
		printf ("Error creating shared map (%d)\n", errno);
		return -ENOMEM;
	}
	return 0;
}

int
subscribe (void)
{
	int ret, fd_register;
	pid_t pid;
	char buffer[32];
	char sync_file_path[32];

	pid = getpid ();
	sprintf (buffer, "%d", pid);

	sprintf (sync_file_path, "/proc/%s", SUBSCRIBE_FILE);
	printf ("# writing <%s> to <%s> (%zu)\n", buffer, sync_file_path, strlen (buffer));

	fd_register = open (sync_file_path, O_TRUNC | O_WRONLY);
	if (fd_register < 0) {
		perror ("opening failed");
		return errno;
	}

	ret = write (fd_register, buffer, strlen (buffer));

	if (ret < 0) {
		perror ("writing failed");
		return errno;
	}

	close (fd_register);
	return 0;
}

void
save_local_timers (void)
{
	float duplex   = (float)(end.tv_nsec - start_app.tv_nsec) / 1000.0;
	float kernel   = (float)(end.tv_nsec - start_kernel.tv_nsec) / 1000.0;
	float app      = duplex - kernel;
	float sig_time = (float)get_sig_info_time_ns () / 1000.0;

	sprintf (timer_array[sig_counter], "%f\t %f\t %f\t %f\n", duplex, app, kernel, sig_time);
}

void
print_local_timers (void)
{
	for (size_t i = 0; i < sig_counter; i++) {
		printf ("%s", timer_array[i]);
	}
}

void
sig_event_handler (int n, siginfo_t *info, void *unused)
{
	clock_gettime (CLOCK_MONOTONIC, &start_app);
	write_to_mmap (send_buffer, MAX_DATA_OBJECT_LEN);
	clock_gettime (CLOCK_MONOTONIC, &start_kernel);
	read_from_mmap (recv_buffer, MAX_DATA_OBJECT_LEN);
	clock_gettime (CLOCK_MONOTONIC, &end);

	if (strcmp (send_buffer, recv_buffer) != 0) {
		printf ("string compare failed (loop %d):\n", sig_counter);
		printf ("    send: %s\n", send_buffer);
		printf ("    recv: %s\n", recv_buffer);
		exit (EXIT_FAILURE);
	}
	if (sig_counter >= MAX_LOOP) {
		print_local_timers ();
		exit (EXIT_SUCCESS);
	}
	save_local_timers ();

	sig_counter++;
	sched_yield ();
}

int
prepare_sig_handler (void)
{
	/* prepare signal handler */
	sig.sa_sigaction = sig_event_handler; // Callback function
	sig.sa_flags	 = SA_SIGINFO;

	/* see sysfs_common.h: SIGRTMIN+6 is different from KERNEL to USERSPACE? */
	sigaction (SIG_SYNC - 2, &sig, NULL);

	sprintf (sig_file_path, "/proc/%s", SIG_FILE);
	return 0;
}

int
main (int argc, char **argv)
{
	if (set_schedule (SCHED_DEADLINE, 90, 30, 100, 100) != 0) {
		printf ("error setting schedule policy (%d)\n", errno);
		exit (EXIT_FAILURE);
	}

	if (subscribe () != 0) {
		printf ("error subscribing: %d", errno);
		exit (EXIT_FAILURE);
	}

	if (prepare_mmap_file () != 0) {
		printf ("error preparing mmap file\n");
		close (fd);
		exit (EXIT_FAILURE);
	}

	if (do_mmap () != 0) {
		printf ("error mmap'ing\n");
		close (fd);
		exit (EXIT_FAILURE);
	}

	generate_random_string (send_buffer, MAX_DATA_OBJECT_LEN);
	prepare_sig_handler ();

	while (running && sig_counter < MAX_LOOP) {
	}

	printf ("# printing time consumption in microseconds\n");
	printf ("# full         \tapp        \tkernel       \tsignal time  \tkernel-user diff    "
		"\n");

	munmap (mem, MEMORY_AREA_LEN);
	close (fd);

	return 0;
}
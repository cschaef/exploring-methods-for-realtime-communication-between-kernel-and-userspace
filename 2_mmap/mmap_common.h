
/*
 *
 */

#ifndef MMAP_COMMON_H
#define MMAP_COMMON_H

#ifdef __KERNEL__
	#include <linux/types.h>
	#include <linux/sched.h>

	#define __u32 u32
	#define __u64 u64
	#define __s32 s32
#endif

#include <stdbool.h>

#define MAX_BUFFER_LEN	  128
#define MMAP_FILE	  "/proc/mmap_lkm"
#define MODULE_NAME	  "mmap_lkm"
#define SUBSCRIBE_FILE	  "mmap_subscribe"
#define SIG_FILE	  "mmap_sig_time"
#define PERMISSIONS	  0666
#define CYCLE_INTERVAL_US 100
#define SCHED_DEADLINE	  6
#define MEMORY_AREA_LEN	  4096
#define READ_AREA_OFF	  0
#define WRITE_AREA_OFF	  MEMORY_AREA_LEN / 2
#define WRITE_AREA_LEN	  MEMORY_AREA_LEN / 2
#define TIME_PERIOD_US	  1000 * 5000 // all 5 seconds
#define TIME_RUNTIME_US	  500
#define SCHED_PRIORITY	  90
#define SIG_SYNC	  (SIGRTMIN + 6) // THIS IS DIFFERENT FROM USERSPACE (40), KERNELSPACE (38) - WHY??

/* Applications point of view:
       ^ +-------+
       : |       | READ
REGION : |       | AREA (Starts w/ READ_AREA_OFF)
SIZE:  : |       | 0 ... 2047
4096   : |-------|--------------------------------
Byte   : |       | WRITE
       : |       | AREA (Starts w/ WRITE_AREA_OFF)
       : |       | 2048 - 4095
       V +-------+
*/

#endif
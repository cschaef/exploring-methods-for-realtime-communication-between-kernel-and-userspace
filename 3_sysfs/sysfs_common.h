#define FILE_AMOUNT	  128
#define SUBSET_AMOUNT	  32
#define FILES_PER_DIR	  (FILE_AMOUNT / 4)
#define kset_name	  "sysfs_lkm"
#define SYNC_FILE	  "sync"
#define sysfs_path	  "/sys/kernel"
#define CYCLE_INTERVAL_MS 100
#define SIG_SYNC	  (SIGRTMIN + 6) // THIS IS DIFFERENT FROM USERSPACE (40), KERNELSPACE (38) - WHY??
char *subset_names[SUBSET_AMOUNT]
	= { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",  "l",  "m",	"n",  "o",  "p",
	    "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "aa", "bb", "cc", "dd", "ee", "ff" };
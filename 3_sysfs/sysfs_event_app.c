/**
 * @file sysfs_event_app.c
 * @brief test app which waits for read/write events on a file descriptor
 * @build: gcc sysfs_event_app.c -o sysfs_event_app.out
 */

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <sys/epoll.h> // epoll_create1(), epoll_ctl(), struct epoll_event
#include <sys/stat.h>  /* for stat */
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define PROGRAM_NAME "sysfs_event_app"

#include "../include/app_common.h"
#include "../include/sig_common.h"
#include "sysfs_common.h"

const char *sig_file_path  = "/sys/kernel/sysfs_lkm/sync/timestamp";
const char *sync_file_path = "/sys/kernel/sysfs_lkm/sync/subscriber_pid";
const char *data_file_path = "/sys/kernel/sysfs_lkm/ff/data_1";

#define MAX_LOOP 10000

int sig_counter = 0;
int running	= 1;
int fd_data, fd_log;
struct timespec start_app, start_kernel, end, kernel_time;
char send_buffer[MAX_DATA_OBJECT_LEN];
char recv_buffer[MAX_DATA_OBJECT_LEN];
char time_buffer[128];
char timer_array[MAX_LOOP][128];

int
send (const char *buffer, size_t len)
{
	lseek (fd_data, 0, SEEK_SET);
	return write (fd_data, send_buffer, len);
}

int
recv (char *buffer, size_t len)
{
	lseek (fd_data, 0, SEEK_SET);
	return read (fd_data, recv_buffer, len);
}

long
get_sig_info_time_ns (void)
{
	char signal_time_buffer[32];
	int sig_info_fd = open (sig_file_path, O_RDONLY);
	lseek (sig_info_fd, 0, SEEK_SET);
	read (sig_info_fd, signal_time_buffer, 4);
	close (sig_info_fd);
	return strtol (signal_time_buffer, NULL, 10);
}

void
print_local_timers (void)
{
	for (size_t i = 0; i < sig_counter; i++) {
		printf ("%s", timer_array[i]);
	}
}

void
save_local_timers (void)
{
	float duplex	       = (float)(end.tv_nsec - start_app.tv_nsec) / 1000.0;
	float kernel	       = (float)(end.tv_nsec - start_kernel.tv_nsec) / 1000.0;
	float app	       = duplex - kernel;
	float user_kernel_diff = (float)(start_app.tv_nsec - kernel_time.tv_nsec) / 1000.0;
	float sig_time	       = (float)get_sig_info_time_ns () / 1000.0;

	sprintf (timer_array[sig_counter], "%f\t %f\t %f\t %f\n", duplex, app, kernel, sig_time);
}

/* add semaphore for this function */
void
sig_event_handler (int n, siginfo_t *info, void *unused)
{
	/* read timestamp from sync/timestamp file */
	recv (time_buffer, 16);
	kernel_time.tv_nsec = strtoll (time_buffer, NULL, 10);

	/* start regular RR routine */
	clock_gettime (CLOCK_MONOTONIC, &start_app);
	send (send_buffer, MAX_DATA_OBJECT_LEN);
	clock_gettime (CLOCK_MONOTONIC, &start_kernel);
	recv (recv_buffer, MAX_DATA_OBJECT_LEN);
	clock_gettime (CLOCK_MONOTONIC, &end);

	if (strcmp (send_buffer, recv_buffer) != 0) {
		printf ("string compare failed (loop %d):\n", sig_counter);
		printf ("    send: %s\n", send_buffer);
		printf ("    recv: %s\n", recv_buffer);
		exit (EXIT_FAILURE);
	}
	if (sig_counter >= MAX_LOOP) {
		print_local_timers ();
		exit (EXIT_SUCCESS);
	}
	save_local_timers ();
	// printf("start_app.tv_nsec: %ld \t kernel_time.tv_nsec: %lld \t sig.si_info: %d\n",
	// start_app.tv_nsec, kernel_time.tv_nsec, info->si_int);
	sig_counter++;
	sched_yield();
}

int
subscribe (void)
{
	int ret, fd;
	pid_t pid;
	char buffer[32];

	pid = getpid ();
	sprintf (buffer, "%d", pid);

	fd = open (sync_file_path, O_RDWR | O_SYNC);

	if (fd < 0) {
		perror ("opening failed");
		return errno;
	}

	ret = write (fd, buffer, strlen (buffer));

	if (ret < 0) {
		perror ("writing failed");
		return errno;
	}

	close (fd);
	return 0;
}

int
main (int argc, char **argv)
{
	struct sigaction sig;

	if (set_schedule (SCHED_DEADLINE, 90, 30, 100, 100) != 0) {
		printf ("error setting schedule policy (%d)\n", errno);
		exit (EXIT_FAILURE);
	}

	if (subscribe () != 0) {
		printf ("error subscribing: %d", errno);
		return -1;
	}

	fd_data = open (data_file_path, O_RDWR | O_SYNC);

	if (fd_data < 0) {
		printf ("error opening data file or log file\n");
		goto cleanup;
	}

	/* prepare signal handler */
	sig.sa_sigaction = sig_event_handler; // Callback function
	sig.sa_flags	 = SA_SIGINFO;

	/* see sysfs_common.h: SIGRTMIN+6 is different from KERNEL to USERSPACE? */
	sigaction (SIG_SYNC - 2, &sig, NULL);

	/* prepare data */
	generate_random_string (send_buffer, MAX_DATA_OBJECT_LEN);

	while (running && sig_counter < MAX_LOOP) {
	}

	printf ("# printing time consumption in microseconds\n");
	printf ("# full         \tapp        \tkernel       \tsignal time  \tkernel-user diff    "
		"\n");
cleanup:
	close (fd_data);
	return 0;
}
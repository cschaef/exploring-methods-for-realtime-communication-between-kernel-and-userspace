/**
 * @file sysfs_buffer_test.c
 * @brief test app to check the speed of writing one large buffer vs small multiple buffers on sysfs
 */

#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define PROGRAM_NAME "sysfs_buffer_test"

#include "../include/app_common.h"
#include "../include/sig_common.h"
#include "sysfs_common.h"

#define AVAIL_PATHS	    FILE_AMOUNT
#define TEST_CYCLES	    AVAIL_PATHS
#define MIN_DATA_OBJECT_LEN (MAX_DATA_OBJECT_LEN / AVAIL_PATHS) // 2048 / 128 files = 16 B

char paths[AVAIL_PATHS][64];
int fds[AVAIL_PATHS];
int fs_single_file;

int
recv (int fd, char *data, size_t length)
{
	lseek (fd, 0, SEEK_SET);
	return read (fd, data, length);
}

int
send (int fd, const char *data, size_t length)
{
	lseek (fd, 0, SEEK_SET);
	return write (fd, data, length);
}

int
populate_paths (void)
{
	int cnt = 0;
	for (size_t i = 0; i < (AVAIL_PATHS - 4) / 4; i++) {
		for (size_t j = 1; j < 5; j++) {
			sprintf (paths[cnt++], "%s/%s/%s/data_%d", sysfs_path, kset_name,
				 subset_names[i], j);
		}
	}

	/* open paths */
	fs_single_file = open (paths[0], O_RDWR | O_SYNC);
	for (size_t i = 0; i < AVAIL_PATHS; i++) {
		fds[i] = open (paths[i], O_RDWR | O_SYNC);
	}
}

int
close_paths (void)
{
	/* close small files */
	for (size_t i = 0; i < AVAIL_PATHS; i++) {
		close (fds[i]);
	}
}

int
static_measures ()
{
	struct timer_info timer, f_timer;
	struct timer_info big_timers[TEST_CYCLES];
	struct timer_info small_timers[TEST_CYCLES];
	struct timer_info *ptr;

	char *data     = (char *)malloc (MAX_DATA_OBJECT_LEN);
	char *response = (char *)malloc (MAX_DATA_OBJECT_LEN);

	/* populate data chunks */
	generate_random_string (data, MAX_DATA_OBJECT_LEN);

	/* big data test, writes a big chunk (MAX_DATA_OBJECT_LEN 2048 Bytes) to a single file for
	 * TEST_CYCLES times */
	ptr = big_timers;

	sched_yield ();
	for (size_t i = 0; i < TEST_CYCLES; i++) {
		clock_gettime (CLOCK_MONOTONIC, &timer.start);
		send (fs_single_file, data, MAX_DATA_OBJECT_LEN);
		recv (fs_single_file, response, MAX_DATA_OBJECT_LEN);
		clock_gettime (CLOCK_MONOTONIC, &timer.end);
		memcpy (ptr++, &timer, sizeof (timer));

		if (strncmp (data, response, MAX_DATA_OBJECT_LEN) != 0) {
			printf ("send & recv str not equal");
			printf ("send: %s\n", data);
			printf ("recv: %s\n", response);
			return -1;
		}
		sched_yield ();
	}
	close (fs_single_file);

	/* small data test, writes MIN_DATA_OBJECT_LEN (16 Bytes) in 128 different files for
	 * TEST_CYCLES times */
	ptr	 = small_timers;
	int size = MIN_DATA_OBJECT_LEN;

	char str[128];

	sched_yield ();

	for (size_t i = 0; i < AVAIL_PATHS; i++) {
		clock_gettime (CLOCK_MONOTONIC, &timer.start);
		send (fds[i], data, size);
		recv (fds[i], response, size);
		clock_gettime (CLOCK_MONOTONIC, &timer.end);
		memcpy (ptr++, &timer, sizeof (timer));

		if (strncmp (data, response, size) != 0) {
			printf ("send & recv str not equal");
			printf ("send: %s\n", data);
			printf ("recv: %s\n", response);
			return -1;
		}
		sched_yield ();
	}

	/* compare */
	printf ("# Results: \n");
	printf ("# Big (%d B)\t Small (%d B)\t");

	size = MIN_DATA_OBJECT_LEN;

	float big_sum	= 0;
	float small_sum = 0;

	for (size_t i = 0; i < TEST_CYCLES; i++) {
		size += MIN_DATA_OBJECT_LEN;

		float big_cycle_duration_us
			= (big_timers[i].end.tv_nsec - big_timers[i].start.tv_nsec) / 1000.0;
		float small_cycle_duration_us
			= (small_timers[i].end.tv_nsec - small_timers[i].start.tv_nsec) / 1000.0;

		big_sum += big_cycle_duration_us;
		small_sum += small_cycle_duration_us;

		printf ("%f \t %f\n", big_cycle_duration_us, small_cycle_duration_us);
	}

	// printf ("\n# Sum:\n");
	// printf ("# %f \t %f\n", big_sum, small_sum);
}

struct timer_info_w_size {
	struct timespec start;
	struct timespec end;
	int data_size;
};

#define DATA_SIZES_LEN 8

int
dynamic_measures (void)
{
	size_t test_cycles = AVAIL_PATHS;
	int data_sizes[DATA_SIZES_LEN] = { 16, 32, 64, 128, 256, 512, 1024, 2048 };
	struct timer_info_w_size timer;
	struct timer_info_w_size timers[TEST_CYCLES * DATA_SIZES_LEN];
	struct timer_info_w_size *ptr = timers;

	char *data     = (char *)malloc (MAX_DATA_OBJECT_LEN);
	char *response = (char *)malloc (MAX_DATA_OBJECT_LEN);

	/* populate data chunks */
	// generate_random_string (data, MAX_DATA_OBJECT_LEN);

	/* do for each data size ... */
	for (size_t i = 0; i < DATA_SIZES_LEN; i++) {

		/* ... 128 (test_cycles) times a measurement */
		for (size_t j = 0; j < test_cycles; j++) {
			// printf("i: %d, j: %d\n", i, j);
			generate_random_string (data, MAX_DATA_OBJECT_LEN);

			clock_gettime (CLOCK_MONOTONIC, &timer.start);
			send (fs_single_file, data, data_sizes[i]);
			recv (fs_single_file, response, data_sizes[i]);
			clock_gettime (CLOCK_MONOTONIC, &timer.end);
			timer.data_size = data_sizes[i];

			memcpy (ptr++, &timer, sizeof (timer));

			if (strncmp (data, response, data_sizes[i]) != 0) {
				printf ("send & recv str not equal");
				printf ("send: %s\n", data);
				printf ("recv: %s\n", response);
				return -1;
			}
			sched_yield ();
		}
	}

	printf("# printing results with raising size from 16 - 2048 bytes\n");

	/* print results */
	for (size_t i = 0; i < TEST_CYCLES * DATA_SIZES_LEN; i++)
	{
		float cycle_duration_us = ( timers[i].end.tv_nsec - timers[i].start.tv_nsec) / 1000.0;
		printf("%f \t %d\n", cycle_duration_us, timers[i].data_size);
	}
	
}

int
main (int argc, char **argv)
{
	if (set_schedule (SCHED_DEADLINE, 90, 30, 30, 1000) != 0) {
		printf ("error setting schedule policy (%d)\n", errno);
		exit (EXIT_FAILURE);
	}

	populate_paths ();
	// static_measures();
	dynamic_measures ();
	close_paths ();
}
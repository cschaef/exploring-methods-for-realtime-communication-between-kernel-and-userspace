/**
 * @file sysfs_lkm.c
 * @brief generic driver to implement a sysfs directory for swift data exchange
 * @references
 * 	https://www.kernel.org/doc/Documentation/filesystems/sysfs.txt
 * 	https://github.com/torvalds/linux/blob/master/samples/kobject/kset-example.c
 * 	https://www.kernel.org/doc/html/latest/core-api/kobject.html
 *
 * cd emfrcbkau/3_sysfs && sudo mount -o rw,remount / && make && sudo insmod sysfs_lkm.ko
 * echo "test" > /sys/kernel/sysfs_lkm/ff/data_1 && cat /sys/kernel/sysfs_lkm/ff/data_1 && dmesg |
 * tail
 */

#include <linux/init.h>
#include <linux/kobject.h>
#include <linux/ktime.h>
#include <linux/module.h>
#include <linux/sched/signal.h>
#include <linux/signal_types.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/timekeeping.h>
#include <linux/timer.h>
#include <linux/version.h>

#include "../include/sig_common.h"
#include "sysfs_common.h"

/* if set to 1, a timestamp will be written periodically to data_0 */
static u8 __do_periodic_cycle = 1;
static struct timer_list timer;
static struct sync_object *sync_obj;

struct sync_object {
	struct kobject kobj;
	long long timestamp;
	int subscriber_pid;
};

struct sync_object_attribute {
	struct attribute attr;
	ssize_t (*show) (struct sync_object *so, struct sync_object_attribute *attr, char *buf);
	ssize_t (*store) (struct sync_object *so, struct sync_object_attribute *attr,
			  const char *buf, size_t count);
};

#define to_sync_object_attr(x) container_of (x, struct sync_object_attribute, attr);
#define to_sync_object_obj(x)  container_of (x, struct sync_object, kobj);

struct payload_data_object_2 {
	struct kobject kobj;
	char data_1[MAX_DATA_OBJECT_LEN];
	char data_2[MAX_DATA_OBJECT_LEN];
	char data_3[MAX_DATA_OBJECT_LEN];
	char data_4[MAX_DATA_OBJECT_LEN];
	char data_5[MAX_DATA_OBJECT_LEN];
	char data_0[MAX_DATA_OBJECT_LEN];
};

struct payload_data_object_2_attribute {
	struct attribute attr;
	ssize_t (*show) (struct payload_data_object_2 *do2,
			 struct payload_data_object_2_attribute *attr, char *buf);
	ssize_t (*store) (struct payload_data_object_2 *do2,
			  struct payload_data_object_2_attribute *attr, const char *buf,
			  size_t count);
};

#define to_payload_data_object_2_attr(x)                                                           \
	container_of (x, struct payload_data_object_2_attribute, attr)
#define to_payload_data_object_2_obj(x) container_of (x, struct payload_data_object_2, kobj)

/**
 * @brief show function for attribute data_0, this can be used for special attribute handling
 * @param kobj
 * @param attr
 * @param buf dest ptr
 * @return # of bytes written
 */
static ssize_t
data_0_show (struct payload_data_object_2 *do2, struct payload_data_object_2_attribute *attr,
	     char *buf)
{
	pr_debug ("start\n");
	return sprintf (buf, "%s", do2->data_0);
}

/**
 * @brief store function for attribute data_0, this can be used for special attribute handling
 * @return # of bytes written
 */
static ssize_t
data_0_store (struct payload_data_object_2 *do2, struct payload_data_object_2_attribute *attr,
	      const char *buf, size_t count)
{
	pr_info ("start - 1 - writing %zu bytes to %s/%s\n", count, do2->kobj.name,
		 attr->attr.name);

	/* check for overflow */
	if (count > MAX_DATA_OBJECT_LEN) {
		count = MAX_DATA_OBJECT_LEN;
	};

	pr_info ("writing now\n");
	return snprintf (do2->data_0, count, "%s", buf);
}

static ssize_t
show_sync (struct sync_object *so, struct sync_object_attribute *attr, char *buf)
{
	/* so and sync_obj are supposed to be the same, why are they different?
	   workaround to use global sync_obj, not the passed ptr so */

	if (strcmp (attr->attr.name, "timestamp") == 0) {
		return sprintf (buf, "%lld", sync_obj->timestamp);
	} else {
		return sprintf (buf, "%d", sync_obj->subscriber_pid);
	}
}

static ssize_t
store_sync (struct sync_object *so, struct sync_object_attribute *attr, const char *buf,
	    size_t count)
{
	int ret;
	if (strcmp (attr->attr.name, "timestamp") == 0) {
		ret = kstrtoll (buf, 10, &sync_obj->timestamp);
		pr_debug ("update timestamp: %lld\n", sync_obj->timestamp);
	} else {
		ret = kstrtoint (buf, 10, &sync_obj->subscriber_pid);
		pr_info ("new subscriber registered: %d\n", sync_obj->subscriber_pid);
	}

	if (ret < 0) {
		pr_err ("error setting subscriber process id\n");
		return ret;
	}

	return count;
}

static void
sync_object_release (struct kobject *kobj)
{
	struct sync_object *so;
	so = to_sync_object_obj (kobj);
	kfree (so);
}

static ssize_t
sync_object_show (struct kobject *kobj, struct attribute *attr, char *buf)
{
	struct sync_object_attribute *attribute;
	struct sync_object *so;

	pr_debug ("start\n");

	attribute = to_sync_object_attr (attr);
	so	  = to_sync_object_obj (kobj);

	if (!attribute->show) {
		return -EIO;
	}

	return attribute->show (so, attribute, buf);
}

static ssize_t
sync_object_store (struct kobject *kobj, struct attribute *attr, const char *buf, size_t len)
{
	struct sync_object_attribute *attribute;
	struct sync_object *so;

	pr_debug ("start\n");

	attribute = to_sync_object_attr (attr);
	so	  = to_sync_object_obj (kobj);

	if (!attribute->store) {
		return -EIO;
	}

	return attribute->store (so, attribute, buf, len);
}

/**
 * @brief release function for a kobj, unravels the kobj holder and kfree()'s it
 * @param kobj ptr
 */
static void
payload_data_object_2_release (struct kobject *kobj)
{
	struct payload_data_object_2 *do2;
	do2 = to_payload_data_object_2_obj (kobj);
	kfree (do2);
}

/**
 * @brief
 * @param do2
 * @param attr
 * @param buf
 * @return
 */
static ssize_t
payload_data_object_2_show (struct kobject *kobj, struct attribute *attr, char *buf)
{
	struct payload_data_object_2_attribute *attribute;
	struct payload_data_object_2 *do2;

	pr_debug ("start\n");

	attribute = to_payload_data_object_2_attr (attr);
	do2	  = to_payload_data_object_2_obj (kobj);

	if (!attribute->show)
		return -EIO;

	return attribute->show (do2, attribute, buf);
}

/**
 * @brief
 * @param do2
 * @param attr
 * @param buf
 * @return
 */
static ssize_t
payload_data_object_2_store (struct kobject *kobj, struct attribute *attr, const char *buf,
			     size_t len)
{
	struct payload_data_object_2_attribute *attribute;
	struct payload_data_object_2 *do2;

	pr_debug ("start\n");

	attribute = to_payload_data_object_2_attr (attr);
	do2	  = to_payload_data_object_2_obj (kobj);

	if (!attribute->store) {
		pr_err ("store callback function not available\n");
		return -EIO;
	}

	return attribute->store (do2, attribute, buf, len);
}

/**
 * @brief generic show routine to return the contents of any attribute
 * @param do2 do ptr which is currently being accessed
 * @param attr its param
 * @param buf destination ptr to write the contents to
 * @return # of bytes written
 */
static ssize_t
show (struct payload_data_object_2 *do2, struct payload_data_object_2_attribute *attr, char *buf)
{
	char *temp_data;
	pr_debug ("start\n");

	if (strcmp (attr->attr.name, "data_1") == 0) {
		temp_data = do2->data_1;
	} else if (strcmp (attr->attr.name, "data_2") == 0) {
		temp_data = do2->data_2;
	} else if (strcmp (attr->attr.name, "data_3") == 0) {
		temp_data = do2->data_3;
	} else if (strcmp (attr->attr.name, "data_4") == 0) {
		temp_data = do2->data_4;
	} else {
		temp_data = do2->data_5;
	}

	return sprintf (buf, "%s", temp_data);
}

/**
 * @brief genric write routine to save a buffer in an attribute
 * @param do2 do ptr which is being accessed
 * @param attr its attributes
 * @param buf source ptr
 * @param count
 * @return # of bytes written
 */
static ssize_t
store (struct payload_data_object_2 *do2, struct payload_data_object_2_attribute *attr,
       const char *buf, size_t count)
{
	/* check for overflow */
	if (count > MAX_DATA_OBJECT_LEN) {
		count = MAX_DATA_OBJECT_LEN;
	}

	pr_debug ("start writing %zu bytes to %s\n", count, attr->attr.name);

	if (strcmp (attr->attr.name, "data_1") == 0)
		strncpy (do2->data_1, buf, count);
	else if (strcmp (attr->attr.name, "data_2") == 0)
		strncpy (do2->data_2, buf, count);
	else if (strcmp (attr->attr.name, "data_3") == 0)
		strncpy (do2->data_3, buf, count);
	else if (strcmp (attr->attr.name, "data_4") == 0)
		strncpy (do2->data_4, buf, count);
	else if (strcmp (attr->attr.name, "data_5") == 0)
		strncpy (do2->data_5, buf, count);
	else
		strncpy (do2->data_0, buf, count);
	return count;
}

/* define the operation routines for this module */
static const struct sysfs_ops sysfs_operations
	= { .show = payload_data_object_2_show, .store = payload_data_object_2_store };

static const struct sysfs_ops sysfs_operations_sync
	= { .show = sync_object_show, .store = sync_object_store };

/* bypass rw permissions to access sysfs files with sudo, should be adapted  */
#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)

/* declare attributes and set their callback functions */
static struct payload_data_object_2_attribute data_1_attribute = __ATTR (data_1, 0644, show, store);
static struct payload_data_object_2_attribute data_2_attribute = __ATTR (data_2, 0644, show, store);
static struct payload_data_object_2_attribute data_3_attribute = __ATTR (data_3, 0644, show, store);
static struct payload_data_object_2_attribute data_4_attribute = __ATTR (data_4, 0644, show, store);
static struct payload_data_object_2_attribute data_5_attribute = __ATTR (data_5, 0644, show, store);
static struct payload_data_object_2_attribute data_0_attribute
	= __ATTR (data_0, 0644, data_0_show, data_0_store);

static struct sync_object_attribute timestamp_attribute
	= __ATTR (timestamp, 0644, show_sync, store_sync);
static struct sync_object_attribute subscriber_pid_attribute
	= __ATTR (subscriber_pid, 0644, show_sync, store_sync);

static struct attribute *sync_object_default_attrs[] = {
	&timestamp_attribute.attr,
	&subscriber_pid_attribute.attr,
	NULL,
};
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 1, 21)
ATTRIBUTE_GROUPS (sync_object_default);
#endif

/*
 * Create a group of attributes so that we can create and destroy them all
 * at once.
 */
static struct attribute *payload_data_object_2_default_attrs[] = {
	&data_0_attribute.attr,
	&data_1_attribute.attr,
	&data_2_attribute.attr,
	&data_3_attribute.attr,
	&data_4_attribute.attr,
	&data_5_attribute.attr,
	NULL, /* need to NULL terminate the list of attributes */
};
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 1, 21)
ATTRIBUTE_GROUPS (payload_data_object_2_default);
#endif

/*
 * Our own ktype for our kobjects.  Here we specify our sysfs ops, the
 * release function, and the set of default attributes we want created
 * whenever a kobject of this type is registered with the kernel.
 */
static struct kobj_type payload_data_object_2_ktype
	= { .sysfs_ops = &sysfs_operations,
	    .release   = payload_data_object_2_release,
#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 1, 21)
	    .default_attrs = payload_data_object_2_default_attrs
#else
	    .default_groups				      = payload_data_object_2_default_groups
#endif
	  };

static struct kobj_type sync_object_ktype = { .sysfs_ops = &sysfs_operations_sync,
					      .release	 = sync_object_release,
#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 1, 21)
					      .default_attrs = sync_object_default_attrs
#else
					      .default_groups = sync_object_default_groups
#endif
};

static struct payload_data_object_2 *do2_objects[SUBSET_AMOUNT];
static struct kset *example_kset;

static struct sync_object *
create_sync_object (const char *so_name)
{
	struct sync_object *so;
	int retval;

	so = kzalloc (sizeof (*so), GFP_KERNEL);
	if (!so) {
		pr_err ("error allocating memory for data object\n");
		return NULL;
	}

	so->kobj.kset = example_kset;
	retval	      = kobject_init_and_add (&so->kobj, &sync_object_ktype, NULL, "%s", so_name);
	if (retval) {
		kobject_put (&so->kobj);
		return NULL;
	}
	kobject_uevent (&so->kobj, KOBJ_ADD);

	pr_info ("create obj: <%s>\n", so->kobj.name);

	return so;
}

static struct payload_data_object_2 *
create_payload_data_object_2 (const char *do2_name)
{
	struct payload_data_object_2 *do2;
	int retval;

	/* allocate the memory for the whole object */
	do2 = kzalloc (sizeof (*do2), GFP_KERNEL);
	if (!do2) {
		pr_err ("error allocating memory for data object\n");
		return NULL;
	}

	/*
	 * As we have a kset for this kobject, we need to set it before calling
	 * the kobject core.
	 */
	do2->kobj.kset = example_kset;

	/*
	 * Initialize and add the kobject to the kernel.  All the default files
	 * will be created here.  As we have already specified a kset for this
	 * kobject, we don't have to set a parent for the kobject, the kobject
	 * will be placed beneath that kset automatically.
	 */
	retval = kobject_init_and_add (&do2->kobj, &payload_data_object_2_ktype, NULL, "%s",
				       do2_name);
	if (retval) {
		kobject_put (&do2->kobj);
		return NULL;
	}

	/*
	 * We are always responsible for sending the uevent that the kobject
	 * was added to the system.
	 */
	kobject_uevent (&do2->kobj, KOBJ_ADD);
	pr_info ("create obj: <%s>\n", do2->kobj.name);

	return do2;
}

static void
destroy_sync_object (struct sync_object *so)
{
	kobject_put (&so->kobj);
}

static void
destroy_payload_data_object_2 (struct payload_data_object_2 *do2)
{
	kobject_put (&do2->kobj);
}

static int
populate_sysfs (void)
{
	size_t i;

	/*
	 * Create a kset with the name of "SYSFS_LKM",
	 * located under /sys/kernel/
	 */
	example_kset = kset_create_and_add (kset_name, NULL, kernel_kobj);
	if (!example_kset)
		return -ENOMEM;

	for (i = 0; i < SUBSET_AMOUNT; i++) {
		do2_objects[i] = create_payload_data_object_2 (subset_names[i]);

		if (do2_objects[i] == NULL) {
			pr_err ("error initializing kobject <%s>\n", subset_names[i]);
			return -ENOMEM;
		}
	}

	/* additionally, prepare sync obj*/
	sync_obj = create_sync_object (SYNC_FILE);

	return 0;
}

static int
sync_subscriber_process (void)
{
	ktime_t sync_timer_1, sync_timer_2;
	struct task_struct *task;
	struct pid *pid_struct = find_get_pid (sync_obj->subscriber_pid);

	if (pid_struct == NULL) {
		pr_debug ("no subscriber pid registered\n");
		return 0;
	}
	task = pid_task (pid_struct, PIDTYPE_PID);

#if LINUX_VERSION_CODE <= KERNEL_VERSION(4, 19, 71)
	struct siginfo info;
	memset (&info, '\0', sizeof (struct siginfo));
#else
	struct kernel_siginfo info;
	memset (&info, '\0', sizeof (struct kernel_siginfo));
#endif

	info.si_signo = SIG_SYNC;
	info.si_code  = SI_QUEUE;

	sync_timer_1 = ktime_get ();
	if (send_sig_info (info.si_signo, &info, task) < 0) {
		pr_err ("error sending signal\n");
		return -1;
	}
	sync_timer_2 = ktime_get ();

	sync_obj->timestamp = sync_timer_2 - sync_timer_1;

	// pr_info ("sending signal %d, sig duration: %lld\n", SIG_SYNC, sync_obj->timestamp);

	return 0;
}

/**
 * periodic routine to write a timestamp to data_0 attribute of the first do2 object
 */
static void
start_event_worker (struct timer_list *unused)
{
	sync_subscriber_process ();
	mod_timer (&timer, jiffies + usecs_to_jiffies (CYCLE_INTERVAL_MS));
}

/**
 * Entry routine, calls sysfs init and sets scheduling policy for this module
 * @return
 */
static int __init
run (void)
{
	int ret = populate_sysfs ();
	if (ret < 0) {
		pr_err ("error populating sysfs structure (%d)\n", ret);
		return -ENOMEM;
	}

	ret = set_scheduling_policy ();
	if (ret < 0) {
		pr_err ("error setting scheduling policy (%d)\n", ret);
	}

	if (__do_periodic_cycle == 1) {
		timer_setup (&timer, start_event_worker, 0);
		mod_timer (&timer, jiffies + usecs_to_jiffies (CYCLE_INTERVAL_MS));
	}

	pr_info ("init done\n");
	return 0;
}

/**
 * @brief exit routine to cleanup sysfs structure
 */
static void __exit
cleanup (void)
{
	size_t i;

	for (i = 0; i < SUBSET_AMOUNT; i++) {
		pr_info ("removing %zu\n", i);
		destroy_payload_data_object_2 (do2_objects[i]);
	}
	destroy_sync_object (sync_obj);
	del_timer (&timer);
	kset_unregister (example_kset);
	pr_info ("goodbye\n");
}

module_init (run);
module_exit (cleanup);
MODULE_LICENSE ("GPL v2");
MODULE_AUTHOR ("Greg Kroah-Hartman <greg@kroah.com>");
MODULE_AUTHOR ("Christian Schaefer <cschaefer.itsb-b2018@fh-salzburg.ac.at>");

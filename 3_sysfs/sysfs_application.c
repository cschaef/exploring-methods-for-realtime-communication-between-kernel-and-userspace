/**
 * @file sysfs_application.c
 * @brief userspace app which interacts with the sysfs lkm
 */

#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define PROGRAM_NAME "sysfs_application"

#include "../include/app_common.h"
#include "../include/sig_common.h"
#include "sysfs_common.h"

int fd = 0;
char file_path[256];

int
my_recv (struct payload_data_object *obj)
{
	/* reset fd to the start */
	lseek (fd, 0, SEEK_SET);

	/* read from file */
	return read (fd, obj->data_ptr, obj->data_len);
}

int
my_send (struct payload_data_object *obj)
{
	/* reset fd to the start */
	lseek (fd, 0, SEEK_SET);

	/* write to sys file */
	return write (fd, obj->data_ptr, obj->data_len);
}

int
main (int argc, char **argv)
{
	parse_args (argc, argv);

	struct timer_info *timers = NULL;
	struct payload_data_object data
		= { .data_len = g_len, .data_ptr = (char *)malloc (g_len), .status = 0 };

	/* init functions */
	generate_random_data (&data);
	if (set_schedule (g_schedule_policy, 90, g_process_time, g_deadline_time, g_period_time)
	    != 0) {
		printf ("error setting schedule policy (%d)\n", errno);
		exit (EXIT_FAILURE);
	}

	sprintf (file_path, "%s/%s/%s/%s", sysfs_path, kset_name, subset_names[0], "data_0");
	fd     = open (file_path, O_RDWR);
	timers = (struct timer_info *)malloc (sizeof (struct timer_info) * g_test_cycles);

	/* start rr sequence test */
	worker_thread (&data, timers, my_send, my_recv);

	/* print results and clean up */
	print_timers (timers, g_test_cycles);
	close (fd);
	free (timers);
	return 0;
}